<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK;

/**
 * Class Client
 * @package SergeR\Webasyst\AxilogSDK
 */
class Client
{
    protected \SergeR\Webasyst\AxilogSDK\Config $config;

    /**
     * Client constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @return Integration\Client
     */
    public function getIntegration(): Integration\Client
    {
        return new Integration\Client($this->config);
    }

}
