<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019-2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK;

/**
 * Class waNet
 * @package SergeR\Webasyst\AxilogSDK
 */
class waNet extends \waNet
{
    public function __construct($options = [], $custom_headers = [])
    {
        parent::__construct($options, $custom_headers);
        $this->options = array_merge($this->options, $options);
    }

    /**
     * @param array $options
     * @return $this
     */
    public function setOptions(array $options): waNet
    {
        $this->options = array_merge($this->options, $options);
        return $this;
    }
}
