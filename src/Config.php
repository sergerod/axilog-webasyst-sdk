<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;

/**
 * Class Config
 * @package SergeR\Webasyst\AxilogSDK
 */
class Config implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /** @var string */
    protected string $ukey = '';

    /** @var string */
    protected string $uid = '';

    /** @var waNet|null */
    protected ?waNet $net = null;

    /** @var bool */
    protected bool $test_mode = false;

    /**
     * @param string $ukey
     * @param string $uid
     * @param waNet|null $net
     * @throws \waException
     */
    public function __construct(string $ukey, string $uid, waNet $net = null)
    {
        $this->uid = $uid;
        $this->ukey = $ukey;

        $this->net = $net ?: new waNet(['timeout' => 25]);
    }

    /**
     * @return string
     */
    public function getUkey(): string
    {
        return $this->ukey;
    }

    /**
     * @param string $ukey
     * @return Config
     */
    public function setUkey(string $ukey): Config
    {
        $this->ukey = $ukey;
        return $this;
    }

    /**
     * @return string
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     * @return Config
     */
    public function setUid(string $uid): Config
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return waNet|null
     */
    public function getNet(): ?waNet
    {
        return clone $this->net;
    }

    /**
     * @param waNet|null $net
     * @return Config
     */
    public function setNet(\SergeR\Webasyst\AxilogSDK\waNet $net): Config
    {
        $this->net = $net;
        return $this;
    }

    /**
     * @return bool
     */
    public function isTestMode(): bool
    {
        return $this->test_mode;
    }

    /**
     * @param bool $test_mode
     * @return $this
     */
    public function setTestMode(bool $test_mode): Config
    {
        $this->test_mode = $test_mode;
        return $this;
    }

    /**
     * @return LoggerInterface|null
     */
    public function getLogger(): ?LoggerInterface
    {
        return $this->logger;
    }
}
