<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Interfaces;

/**
 * Interface ArraySerializable
 * @package SergeR\Webasyst\AxilogSDK\Interfaces
 */
interface ArraySerializable
{
    /**
     * @return array
     */
    public function toArray(): array;
}
