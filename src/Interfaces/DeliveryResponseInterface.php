<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license
 */

namespace SergeR\Webasyst\AxilogSDK\Interfaces;

use SergeR\Webasyst\AxilogSDK\Integration\Entity\ResponseStatus;

/**
 * Interface DeliveryResponseInterface
 * @package SergeR\Webasyst\AxilogSDK\Interfaces
 */
interface DeliveryResponseInterface
{
    /**
     * @return string|null
     */
    public function getObjectId();

    /**
     * @return string|null
     */
    public function getObjectKey();

    /**
     * @return float|null
     */
    public function getPrice();

    /**
     * @return ResponseStatus
     */
    public function getStatus();

    /**
     * @return string[]
     */
    public function getWarnings();
}
