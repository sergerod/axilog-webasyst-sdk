<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Interfaces;

/**
 * Interface DeliveryOrder
 * @package SergeR\Webasyst\AxilogSDK\Interfaces
 */
interface DeliveryOrder
{
    public function getOkey();
}