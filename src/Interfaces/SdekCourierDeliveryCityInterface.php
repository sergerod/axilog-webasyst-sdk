<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Interfaces;

/**
 * Interface SdekCourierDeliveryCityInterface
 * @package SergeR\Webasyst\AxilogSDK\Interfaces
 */
interface SdekCourierDeliveryCityInterface
{
    public function getCode(): int;

    public function setCode(int $code);

    public function getRegionName(): string;

    public function setRegionName(string $regionName);

    public function getName(): string;

    public function setName(string $name);
}
