<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Interfaces;

use SergeR\Webasyst\AxilogSDK\Integration\Entity\FivePostCellLimits;

/**
 * Interface FivepostPickupInterface
 * @package SergeR\Webasyst\AxilogSDK\Interfaces
 */
interface FivepostPickupInterface
{
    public function __construct();

    public function getCode(): string;

    public function setCode(string $code);

    public function getRegion(): string;

    public function setRegion(string $region);

    public function getRegionType(): string;

    public function setRegionType(string $regionType);

    public function getCity(): string;

    public function setCity(string $city);

    public function getFullAddress(): string;

    public function setFullAddress(string $fullAddress);

    public function getPhone(): string;

    public function setPhone(string $phone);

    public function getLat(): ?float;

    public function setLat(?float $lat);

    public function getLong(): ?float;

    public function setLong(?float $long);

    public function isCashAllowed(): bool;

    public function setCashAllowed(bool $cashAllowed);

    public function isCardAllowed(): bool;

    public function setCardAllowed(bool $cardAllowed);

    public function getCellLimits(): FivePostCellLimits;

    public function setCellLimits(FivePostCellLimits $cellLimits);

    public function getType(): string;

    public function setType(string $type);

    public function getAdditional(): string;

    public function setAdditional(string $additional);
}
