<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Interfaces;

use SergeR\Webasyst\AxilogSDK\Integration\Entity\SdekWeightLimit;

/**
 * Interface SdekPickupInterface
 * @package SergeR\Webasyst\AxilogSDK\Interfaces
 */
interface SdekPickupInterface
{
    public function __construct();

    public function getCode(): string;

    public function setCode(string $code);

    public function getRegionName(): string;

    public function setRegionName(string $regionName);

    public function getCityCode(): int;

    public function setCityCode(int $cityCode);

    public function getAddress(): string;

    public function setAddress(string $address);

    public function getFullAddress(): string;

    public function setFullAddress(string $fullAddress);

    public function getPhone(): string;

    public function setPhone(string $phone);

    public function getWorkTime(): string;

    public function setWorkTime(string $workTime);

    public function getCoordX(): ?float;

    public function setCoordX(?float $coordX);

    public function getCoordY(): ?float;

    public function setCoordY(?float $coordY);

    public function isDressingRoom(): bool;

    public function setIsDressingRoom(bool $isDressingRoom);

    public function isHaveCashless(): bool;

    public function setHaveCashless(bool $haveCashless);

    public function isAllowedCod(): bool;

    public function setAllowedCod(bool $allowedCod);

    public function getAddressComment(): string;

    public function setAddressComment(string $addressComment);

    public function getWeightLimit(): SdekWeightLimit;

    public function setWeightLimit(SdekWeightLimit $weightLimit);
}
