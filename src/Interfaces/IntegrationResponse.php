<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license
 */

namespace SergeR\Webasyst\AxilogSDK\Interfaces;

/**
 * Interface IntegrationResponse
 * @package SergeR\Webasyst\AxilogSDK\Integration
 */
interface IntegrationResponse
{
    /**
     * @return \SimpleXMLElement
     */
    public function getRawResponse(): \SimpleXMLElement;
}
