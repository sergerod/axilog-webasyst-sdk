<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2021
 * @license MIT
 */
declare(strict_types=1);

namespace SergeR\Webasyst\AxilogSDK\Interfaces;
/**
 * Class DeliveryRequest
 * @package SergeR\Webasyst\AxilogSDK\Interfaces
 */
interface DeliveryRequest
{

}
