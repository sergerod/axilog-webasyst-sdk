<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020-2021
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration;

use SergeR\CakeUtility\Exception\XmlException;

/**
 * Class AbstractRequest
 * @package SergeR\Webasyst\AxilogSDK\Integration
 */
abstract class AbstractRequest
{
    protected $_root = 'singleorder';

    protected $_auth_required=true;

    /**
     * @param Client $client
     * @return mixed
     * @throws XmlException
     * @throws \waException
     */
    public function send(Client $client)
    {
        $result = $client->send($this->_getMode(), $this->_build(), $this->isAuthRequired(), $this->_root);

        return $this->_decodeResponse($result);
    }

    /**
     * @return string
     */
    abstract protected function _getMode(): string;

    abstract protected function _decodeResponse(\SimpleXMLElement $result);

    /**
     * @return bool
     */
    public function isAuthRequired(): bool
    {
        return $this->_auth_required;
    }

    /**
     * @param bool $auth_required
     * @return $this
     */
    public function setAuthRequired(bool $auth_required)
    {
        $this->_auth_required = $auth_required;
        return $this;
    }

    protected function _build(): array
    {
        return [];
    }
}
