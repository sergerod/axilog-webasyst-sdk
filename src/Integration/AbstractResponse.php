<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration;

use SergeR\Webasyst\AxilogSDK\Interfaces\IntegrationResponse;
use SimpleXMLElement;

/**
 * Class AbstractResponse
 * @package SergeR\Webasyst\AxilogSDK\Integration
 */
abstract class AbstractResponse implements IntegrationResponse
{
    protected \SimpleXMLElement $response;

    public function __construct(SimpleXMLElement $response)
    {
        $this->response = $response;
    }

    /**
     * @return SimpleXMLElement
     */
    public function getRawResponse(): SimpleXMLElement
    {
        return $this->response;
    }
}
