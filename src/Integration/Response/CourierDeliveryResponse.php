<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Response;

use SergeR\CakeUtility\Hash;
use SergeR\CakeUtility\Xml;
use SergeR\Webasyst\AxilogSDK\Integration\AbstractResponse;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\ResponseStatus;
use SergeR\Webasyst\AxilogSDK\Interfaces\DeliveryResponseInterface;
use SergeR\Webasyst\AxilogSDK\Interfaces\IntegrationResponse;

/**
 * Class CourierDeliveryOrder
 * @package SergeR\Webasyst\AxilogSDK\Integration\Response
 */
class CourierDeliveryResponse extends AbstractResponse implements IntegrationResponse, DeliveryResponseInterface
{
    /** @var string|null */
    protected ?string $object_id;

    /** @var string|null */
    protected ?string $object_key;

    /** @var float|null */
    protected ?float $price;

    /** @var ResponseStatus */
    protected ResponseStatus $status;

    /** @var string[] */
    protected array $warnings = [];

    public function __construct(\SimpleXMLElement $response)
    {
        parent::__construct($response);
        $this->_decodeResponse();
    }

    /**
     * @throws \SergeR\CakeUtility\Exception\XmlException
     */
    protected function _decodeResponse()
    {
        $decoded_response = Xml::toArray($this->response);
        $status = Hash::get($decoded_response, 'response.status.@code');
        $status_msg = Hash::get($decoded_response, 'response.status.@');
        $this->status = new ResponseStatus($status === null ? null : (int)$status, $status_msg ? (string)$status_msg : null);
        $this->price = Hash::get($decoded_response, 'response.status.@price');
        if ($this->price !== null) $this->price = (float)$this->price;
        $this->object_id = Hash::get($decoded_response, 'response.auth.@objectid');
        $this->object_key = Hash::get($decoded_response, 'response.auth.@');
        $this->warnings = (array)Hash::get($decoded_response, 'response.warnings.warning');
    }

    /**
     * @return string|null
     */
    public function getObjectId(): ?string
    {
        return $this->object_id;
    }

    /**
     * @return string|null
     */
    public function getObjectKey(): ?string
    {
        return $this->object_key;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @return ResponseStatus
     */
    public function getStatus()
    {
        return clone $this->status;
    }

    /**
     * @return string[]
     */
    public function getWarnings(): array
    {
        return $this->warnings;
    }
}
