<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Response;

use SergeR\Webasyst\AxilogSDK\Integration\AbstractResponse;

/**
 * Class GetLabelResponse
 * @package SergeR\Webasyst\AxilogSDK\Integration\Response
 */
class GetLabelResponse extends AbstractResponse
{
    protected string $html = '';

    public function __construct(\SimpleXMLElement $response)
    {
        if($html = $response->html) $this->html = (string)$html;
        parent::__construct($response);
    }

    /**
     * @return string
     */
    public function getHtml(): string
    {
        return $this->html;
    }
}
