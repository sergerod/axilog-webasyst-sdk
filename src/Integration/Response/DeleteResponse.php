<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Response;

use SergeR\CakeUtility\Exception\XmlException;
use SergeR\CakeUtility\Hash;
use SergeR\CakeUtility\Xml;
use SergeR\Webasyst\AxilogSDK\Integration\AbstractResponse;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\ResponseStatus;

/**
 * Class DeleteResponse
 * @package SergeR\Webasyst\AxilogSDK\Integration\Response
 */
class DeleteResponse extends AbstractResponse
{
    protected $order_id;

    protected \SergeR\Webasyst\AxilogSDK\Integration\Entity\ResponseStatus $status;

    public function __construct(\SimpleXMLElement $response)
    {
        try {
            $data = Xml::toArray($response);
        } catch (XmlException $e) {
            $data = [];
        }
        $order_id = Hash::get($data, 'response.order.@id');
        $status_id = Hash::get($data, 'response.status.@code');

        if($order_id) $this->order_id = $order_id;
        $this->status = new ResponseStatus($status_id == null ? null : (int)$status_id);

        parent::__construct($response);
    }

    /**
     * @return int|null
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * @return ResponseStatus
     */
    public function getStatus(): \SergeR\Webasyst\AxilogSDK\Integration\Entity\ResponseStatus
    {
        return $this->status;
    }
}