<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Response;

use SergeR\Webasyst\AxilogSDK\Interfaces\DeliveryResponseInterface;
use SergeR\Webasyst\AxilogSDK\Interfaces\IntegrationResponse;

/**
 * Class SdekPickupDeliveryResponse
 * @package SergeR\Webasyst\AxilogSDK\Integration\Response
 */
class SdekPickupDeliveryResponse extends CourierDeliveryResponse implements IntegrationResponse, DeliveryResponseInterface
{

}