<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Response;

use Countable;
use DateTimeImmutable;
use SergeR\CakeUtility\Exception\XmlException;
use SergeR\CakeUtility\Hash;
use SergeR\CakeUtility\Xml;
use SergeR\Webasyst\AxilogSDK\Integration\AbstractResponse;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\CalculatedTariff;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\ResponseStatus;
use SimpleXMLElement;

/**
 * Class CalculatedTariffs
 * @package SergeR\Webasyst\AxilogSDK\Integration\Response
 */
class CalculatedTariffs extends AbstractResponse implements Countable
{
    /** @var CalculatedTariff[] */
    protected array $tariffs = [];

    protected ResponseStatus $status;

    public function __construct(SimpleXMLElement $response)
    {
        parent::__construct($response);

        try {
            $data = Xml::toArray($response);
        } catch (XmlException $e) {
            $data = [];
        }

        $status_code = Hash::get($data, 'response.status.@code');
        $status_message = Hash::get($data, 'response.status.@');

        $this->status = new ResponseStatus($status_code === null ? null : (int)$status_code, $status_message);

        if ($alternatives = Hash::get($data, 'response.alternatives')) {
            $alternatives = (array)Hash::get($data, 'response.alternatives.alternative');
            if ($alternatives) {
                if (Hash::dimensions($alternatives) < 2) $alternatives = [$alternatives];
                foreach ($alternatives as $alternative) {
                    if (is_array($alternative)) {
                        $tariff = new CalculatedTariff();
                        if (($value = Hash::get($alternative, '@tarif')) != null) $tariff->setValue((float)$value);
                        if (($name = Hash::get($alternative, '@name')) != null) $tariff->setName($name);
                        if (($post_type = Hash::get($alternative, '@post_type')) != null) $tariff->setPostType((int)$post_type);
                        if ((($date = Hash::get($alternative, '@delivery_date')) !== null) && preg_match("/^\d{2}\.\d{2}\.\d{4}$/ui", (string)$date)) {
                            $date = DateTimeImmutable::createFromFormat("d.m.Y", (string)$date);
                            if ($date) $tariff->setDeliveryDate($date);
                        }

                        $this->tariffs[] = $tariff;
                    }
                }
            }
        } elseif (Hash::get($data, 'response.tarif')) {
            $tariff = new CalculatedTariff;
            if (($value = Hash::get($data, 'response.tarif.@')) != null) $tariff->setValue((float)$value);
            elseif ((($value = Hash::get($data, 'response.tarif')) !== null) && is_scalar($value)) $tariff->setValue((float)$value);
            if ((($date = Hash::get($data, 'response.tarif.@delivery_date')) !== null) && preg_match("/^\d{2}\.\d{2}\.\d{4}$/ui", (string)$date)) {
                $date = DateTimeImmutable::createFromFormat("d.m.Y", (string)$date);
                if ($date) $tariff->setDeliveryDate($date);
            }
            $this->tariffs[] = $tariff;
        }
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return count($this->tariffs);
    }

    /**
     * @return CalculatedTariff[]
     */
    public function getTariffs(): array
    {
        return $this->tariffs;
    }

    /**
     * @return ResponseStatus
     */
    public function getStatus(): ResponseStatus
    {
        return $this->status;
    }
}
