<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Response;

use InvalidArgumentException;
use SergeR\Webasyst\AxilogSDK\Integration\AbstractResponse;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\ResponseStatus;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\SdekCourierDeliveryCity;
use SergeR\Webasyst\AxilogSDK\Interfaces\IntegrationResponse;
use SergeR\Webasyst\AxilogSDK\Interfaces\SdekCourierDeliveryCityInterface;

/**
 * Class SdekCourierCities
 * @package SergeR\Webasyst\AxilogSDK\Integration\Response
 */
class SdekCourierCities extends AbstractResponse implements \Countable, IntegrationResponse
{
    protected ResponseStatus $_status;

    protected string $_city_classname = SdekCourierDeliveryCity::class;

    public function __construct(\SimpleXMLElement $response)
    {
        parent::__construct($response);
        $this->_status = new ResponseStatus($response->status ? (int)$response->status['code'] : null);
    }

    /**
     * @param string $city_classname
     * @return SdekCourierCities
     */
    public function setCityClassname(string $city_classname)
    {
        if (!is_subclass_of($city_classname, SdekCourierDeliveryCityInterface::class))
            throw new InvalidArgumentException("'$city_classname' isn't implements SdekCourierDeliveryCityInterface");

        $this->_city_classname = $city_classname;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return count($this->response->delivery_list->city);
    }

    /**
     * @return \Generator
     */
    public function getCities()
    {
        foreach ($this->response->delivery_list->city as $item) {
            $city = new $this->_city_classname();
            $city->setCode((int)$item['code'])
                 ->setRegionName((string)$item['regionName'])
                 ->setName((string)$item['name']);

            yield $city;
        }
    }

    /**
     * @return ResponseStatus
     */
    public function getStatus(): ResponseStatus
    {
        return $this->_status;
    }
}
