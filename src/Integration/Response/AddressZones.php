<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Response;

use Countable;
use SergeR\CakeUtility\Exception\XmlException;
use SergeR\CakeUtility\Hash;
use SergeR\CakeUtility\Xml;
use SergeR\Webasyst\AxilogSDK\Integration\AbstractResponse;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\AddressZone;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\ResponseStatus;
use SimpleXMLElement;

/**
 * Class AddressZones
 * @package SergeR\Webasyst\AxilogSDK\Integration\Response
 */
class AddressZones extends AbstractResponse implements Countable
{
    /** @var AddressZone[] */
    protected array $zones = [];

    protected \SergeR\Webasyst\AxilogSDK\Integration\Entity\ResponseStatus $status;

    /**
     * AddressZones constructor.
     * @param SimpleXMLElement $response
     */
    public function __construct(SimpleXMLElement $response)
    {
        parent::__construct($response);

        try {
            $data = Xml::toArray($response);
        } catch (XmlException $e) {
            $data = [];
        }

        $status_code = $data['response']['status']['@code'] ?? null;
        $status_message = $data['response']['status']['@'] ?? null;

        $this->status = new ResponseStatus($status_code === null ? null : (int)$status_code, $status_message);

        $zones = (array)($data['response']['zones']['zone'] ?? []);
        if (Hash::dimensions($zones) < 2) $zones = [$zones];

        foreach ($zones as $zone) {
            if (!is_array($zone)) continue;
            $address_zone = (new AddressZone())
                ->setLat(isset($zone["@lat"]) ? (float)$zone["@lat"] : null)
                ->setLong(isset($zone["@long"]) ? (float)$zone["@long"] : null)
                ->setStatus((int)Hash::get($zone, '@status'))
                ->setCity(isset($zone["@city"]) ? (int)$zone["@city"] : null)
                ->setZone(isset($zone["@zone"]) ? (int)$zone["@zone"] : null)
                ->setMessage(trim((string)Hash::get($zone, '@')));

            $this->add($address_zone);
        }
    }

    public function count(): int
    {
        return count($this->zones);
    }

    /**
     * @param AddressZone ...$zones
     * @return $this
     */
    public function add(AddressZone ...$zones)
    {
        foreach ($zones as $zone) $this->zones[] = clone $zone;

        return $this;
    }

    /**
     * @return AddressZone[]
     */
    public function getZones(): array
    {
        return $this->zones;
    }

    /**
     * @return ResponseStatus
     */
    public function getStatus(): \SergeR\Webasyst\AxilogSDK\Integration\Entity\ResponseStatus
    {
        return $this->status;
    }
}
