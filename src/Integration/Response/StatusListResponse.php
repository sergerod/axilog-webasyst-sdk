<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2021
 * @license
 */
declare(strict_types=1);

namespace SergeR\Webasyst\AxilogSDK\Integration\Response;

use SergeR\Webasyst\AxilogSDK\Integration\AbstractResponse;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryStatus;
use SergeR\Webasyst\AxilogSDK\Interfaces\IntegrationResponse;
use SimpleXMLElement;

/**
 * Class StatusListResponse
 * @package SergeR\Webasyst\AxilogSDK\Integration\Response
 */
class StatusListResponse extends AbstractResponse implements IntegrationResponse
{
    /** @var DeliveryStatus[] */
    protected array $statuses = [];

    public function __construct(SimpleXMLElement $response)
    {
        parent::__construct($response);

        if ($response->okeylist && $response->okeylist->okey) {
            foreach ($response->okeylist->okey as $value) {
                if ($value) {
                    $item = (new DeliveryStatus())
                        ->setOrderId((string)$value['id'])
                        ->setOrderInnerId((string)$value['inner_id'])
                        ->setOrderType((string)$value['type'])
                        ->setOkey((string)$value['okey'])
                        ->setStatusCode((int)$value['status_code'])
                        ->setStatusText((string)$value['status_name']);
                    if (strlen($customer_price = (string)$value['customer_price']))
                        $item->setOrderCustomerPrice((float)$customer_price);
                    if (strlen($price = (string)$value['price']))
                        $item->setOrderPrice((float)$price);
                    if (strlen($exe_date = (string)$value['exe_date'])) {
                        if (preg_match('/\d{1,2}\.\d{1,2}\.\d{4}/', $exe_date)) {
                            if ($exe_date = \DateTime::createFromFormat('d.m.Y', $exe_date)->format('Y-m-d'))
                                $item->setExeDate($exe_date);

                        }
                    }
                    if (strlen($payment_mode = (string)$value['payment_mode']))
                        $item->setOrderPaymentMode((int)$payment_mode);

                    if (strlen($tracking = (string)$value['tracking']))
                        $item->setPoststatusTracking($tracking);

                    $this->statuses[] = $item;
                }
            }
        }
    }

    /**
     * @return DeliveryStatus[]
     */
    public function getStatuses(): array
    {
        return $this->statuses;
    }
}
