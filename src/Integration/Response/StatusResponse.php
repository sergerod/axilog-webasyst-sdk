<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020-2021
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Response;

use SergeR\CakeUtility\Exception\XmlException;
use SergeR\CakeUtility\Hash;
use SergeR\CakeUtility\Xml;
use SergeR\Webasyst\AxilogSDK\Integration\AbstractResponse;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryStatus;

/**
 * Class StatusResponse
 * @package SergeR\Webasyst\AxilogSDK\Integration\Response
 */
class StatusResponse extends AbstractResponse
{
    /**
     * @var mixed
     */
    protected $order_id;
    /**
     * @var mixed
     */
    protected $order_inner_id;
    protected float $order_price;
    /**
     * @var
     */
    protected $order_customer_price;
    /**
     * @var
     */
    protected $order_incl_deliv_sum;
    /**
     * @var
     */
    protected $order_group;
    /**
     * @var
     */
    protected $order_export_order;
    /**
     * @var mixed
     */
    protected $order_type;
    /**
     * @var
     */
    protected $order_fid;
    /**
     * @var int
     */
    protected int $order_payment_mode;
    protected int $status_code;
    protected string $status_text;
    protected string $d_date;
    /**
     * @var string
     */
    protected string $poststatus_tracking;
    /**
     * @var float
     */
    protected float $poststatus_price;
    protected \SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryStatus $delivery_status;

    /**
     * StatusResponse constructor.
     * @param \SimpleXMLElement $response
     */
    public function __construct(\SimpleXMLElement $response)
    {
        parent::__construct($response);
        try {
            $data = Xml::toArray($response);
        } catch (XmlException $e) {
            $data = [];
        }

        $this->delivery_status = new DeliveryStatus();

        $this->delivery_status
            ->setOrderId(Hash::get($data, 'response.order.@id'))
            ->setOrderInnerId(Hash::get($data, 'response.order.@inner_id'))
            ->setOrderPrice((float)Hash::get($data, 'response.order.@price'))
            ->setOrderType(Hash::get($data, 'response.order.@type'))
            ->setStatusCode((int)Hash::get($data, 'response.status.@code'))
            ->setStatusText((string)Hash::get($data, 'response.status.@'))
            ->setDDate((string)Hash::get($data, 'response.d_date'));

        if (($payment_mode = Hash::get($data, 'response.order.@payment_mode')) !== null)
            $this->delivery_status->setOrderPaymentMode((int)$payment_mode);

        if (($poststatus_tracking = Hash::get($data, 'response.poststatus.@tracking')) !== null)
            $this->delivery_status->setPoststatusTracking((string)$poststatus_tracking);

        if (($poststatus_price = Hash::get($data, 'response.poststatus.@postprice')) !== null)
            $this->delivery_status->setPoststatusPrice((float)$poststatus_price);
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     * @depecated Only for compatibility with pre-release versions
     */
    public function getOrderInclDelivSum()
    {
        return $this->order_incl_deliv_sum;
    }

    /**
     * @return mixed
     */
    public function getOrderGroup()
    {
        return $this->order_group;
    }

    /**
     * @return mixed
     */
    public function getOrderExportOrder()
    {
        return $this->order_export_order;
    }

    /**
     * @return mixed
     */
    public function getOrderType()
    {
        return $this->order_type;
    }

    /**
     * @return mixed
     */
    public function getOrderFid()
    {
        return $this->order_fid;
    }

    /**
     * @return int
     */
    public function getOrderPaymentMode(): int
    {
        return $this->order_payment_mode;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->status_code;
    }

    /**
     * @return string
     */
    public function getStatusText(): string
    {
        return $this->status_text;
    }

    /**
     * @return string
     */
    public function __call($name, $arguments)
    {
        if ((strpos($name, 'get') === 0) && method_exists(DeliveryStatus::class, $name))
            return $this->delivery_status->{$name}(...$arguments);

        throw new \BadMethodCallException('Method ' . $name . ' does not exist');
    }
}
