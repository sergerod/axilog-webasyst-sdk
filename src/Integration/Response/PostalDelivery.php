<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Response;

use SergeR\Webasyst\AxilogSDK\Integration\AbstractResponse;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\ResponseStatus;
use SergeR\Webasyst\AxilogSDK\Interfaces\DeliveryResponseInterface;
use SergeR\Webasyst\AxilogSDK\Interfaces\IntegrationResponse;

/**
 * Class PostalDelivery
 * @package SergeR\Webasyst\AxilogSDK\Integration\Response
 */
class PostalDelivery extends CourierDeliveryResponse implements IntegrationResponse, DeliveryResponseInterface
{
}
