<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Response;

use Generator;
use InvalidArgumentException;
use SergeR\Webasyst\AxilogSDK\Integration\AbstractResponse;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\FivePostCellLimits;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\FivePostPickup;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\ResponseStatus;
use SergeR\Webasyst\AxilogSDK\Interfaces\FivepostPickupInterface;

/**
 * Class FivePostPickupList
 * @package SergeR\Webasyst\AxilogSDK\Integration\Response
 */
class FivePostPickupList extends AbstractResponse implements \Countable
{
    protected \SergeR\Webasyst\AxilogSDK\Integration\Entity\ResponseStatus $_status;

    /** @var string */
    protected string $_pickup_classname = FivePostPickup::class;

    public function __construct(\SimpleXMLElement $response)
    {
        parent::__construct($response);
        $this->_status = new ResponseStatus($response->status ? (int)$response->status['code'] : null);
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return count($this->response->pickup_list->office);
    }

    /**
     * @param string $pickup_classname
     * @return $this
     */
    public function setPickupClassname(string $pickup_classname): FivePostPickupList
    {
        if (!is_subclass_of($pickup_classname, FivepostPickupInterface::class))
            throw new InvalidArgumentException("'$pickup_classname' isn't implements FivepostPickupInterface");
        $this->_pickup_classname = $pickup_classname;

        return $this;
    }

    /**
     * @return Generator
     */
    public function getPickups()
    {
        foreach ($this->response->pickup_list->office as $item) {
            $pickup = new $this->_pickup_classname();

            $pickup->setCode((string)$item['code'])
                ->setRegion((string)$item['region'])
                ->setRegionType((string)$item['regionType'])
                ->setCity((string)$item['city'])
                ->setFullAddress((string)$item['fullAddress'])
                ->setPhone((string)$item['phone'])
                ->setLat(((float)str_replace(',', '.', (string)$item['lat'])) ?: null)
                ->setLong(((float)str_replace(',', '.', (string)$item['long'])) ?: null)
                ->setCashAllowed((bool)intval((string)$item['cashAllowed']))
                ->setCardAllowed((bool)intval((string)$item['cardAllowed']))
                ->setType((string)$item['type'])
                ->setAdditional((string)$item['additional']);

            if ($item['cellLimits']) {
                $pickup->setCellLimits(new FivePostCellLimits((string)$item['cellLimits']));
            }

            yield $pickup;
        }
    }

    /**
     * @return ResponseStatus
     */
    public function getStatus(): ResponseStatus
    {
        return $this->_status;
    }
}
