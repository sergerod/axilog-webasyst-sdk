<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Response;

use InvalidArgumentException;
use SergeR\Webasyst\AxilogSDK\Integration\AbstractResponse;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\ResponseStatus;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\SdekPickup;
use SergeR\Webasyst\AxilogSDK\Interfaces\SdekPickupInterface;

/**
 * Class SdekPickupList
 * @package SergeR\Webasyst\AxilogSDK\Integration\Response
 */
class SdekPickupList extends AbstractResponse implements \Countable
{
    /** @var \SimpleXMLElement */
    protected \SimpleXMLElement $response;

    protected ResponseStatus $_status;

    protected string $_pickup_classname = SdekPickup::class;

    public function __construct(\SimpleXMLElement $response)
    {
        parent::__construct($response);
        $this->_status = new ResponseStatus($response->status ? (int)$response->status['code'] : null);
    }

    /**
     * @param string $pickup_classname
     * @return $this
     */
    public function setPickupClassname(string $pickup_classname)
    {
        if(!is_subclass_of($pickup_classname, SdekPickupInterface::class))
            throw new InvalidArgumentException("'$pickup_classname' isn't implements SdekPickupInterface");
        $this->_pickup_classname = $pickup_classname;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return count($this->response->pickup_list->office);
    }

    /**
     * @return \Generator
     */
    public function getPickups(): \Generator
    {
        foreach ($this->response->pickup_list->office as $item) {
            $pickup = new $this->_pickup_classname();
            $pickup->setCode((string)$item['code'])
                ->setRegionName((string)$item['regionName'])
                ->setCityCode((int)$item['cityCode'])
                ->setAddress((string)$item['address'])
                ->setFullAddress((string)$item['fullAddress'])
                ->setPhone((string)$item['phone'])
                ->setWorkTime((string)$item['workTime'])
                ->setCoordX(((float)str_replace(',', '.', (string)$item['coordX'])) ?: null)
                ->setCoordY(((float)str_replace(',', '.', (string)$item['coordY'])) ?: null)
                ->setIsDressingRoom((bool)intval((string)$item['isDressingRoom']))
                ->setHaveCashless((bool)intval((string)$item['haveCashless']))
                ->setAllowedCod((bool)intval((string)$item['allowedCod']))
                ->setAddressComment((string)$item['addressComment']);
            $weight_limit = (string)$item['weightLimit'];
            if ($weight_limit && floatval($weight_limit) > 0) {
                $pickup->setWeightLimit((float)$weight_limit);
            }

            yield $pickup;
        }
    }

    /**
     * @return ResponseStatus
     */
    public function getStatus(): ResponseStatus
    {
        return $this->_status;
    }
}
