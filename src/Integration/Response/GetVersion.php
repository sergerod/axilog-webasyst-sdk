<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2021
 * @license MIT
 */
declare(strict_types=1);

namespace SergeR\Webasyst\AxilogSDK\Integration\Response;

use SergeR\Webasyst\AxilogSDK\Integration\AbstractResponse;
use SergeR\Webasyst\AxilogSDK\Interfaces\IntegrationResponse;
use SimpleXMLElement;

/**
 * Class GetVersion
 * @package SergeR\Webasyst\AxilogSDK\Integration\Response
 */
class GetVersion extends AbstractResponse implements IntegrationResponse
{
    protected string $version;

    /**
     * GetVersion constructor.
     * @param SimpleXMLElement $response
     */
    public function __construct(SimpleXMLElement $response)
    {
        parent::__construct($response);

        $this->version = (string)$response->version;
    }

    /**
     * @return string
     */
    public function getVersion(): string
    {
        return $this->version;
    }
}
