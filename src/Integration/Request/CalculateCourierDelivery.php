<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Request;

use SergeR\Webasyst\AxilogSDK\Integration\Client;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\CourierDeliveryOrder;
use SergeR\Webasyst\AxilogSDK\Integration\Response\CalculatedTariffs;

/**
 * Class CalculateCourierDelivery
 * @package SergeR\Webasyst\AxilogSDK\Integration\Request
 * @method getOrder() : CourierDeliveryOrder
 * @method CalculatedTariffs send(Client $client):
 */
class CalculateCourierDelivery extends AbstractCalculationRequest
{
    /**
     * @inheritDoc
     */
    protected function _getMode(): string
    {
        return 'get_tarif_new';
    }

    /**
     * @param CourierDeliveryOrder $order
     * @return $this
     */
    public function setOrder(CourierDeliveryOrder $order)
    {
        $this->order = $order;
        return $this;
    }
}
