<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Request;

use SergeR\Webasyst\AxilogSDK\Integration\Entity\FivepostDeliveryOrder;
use SergeR\Webasyst\AxilogSDK\Integration\Response\FivepostPickupDeliveryResponse;
use SergeR\Webasyst\AxilogSDK\Interfaces\DeliveryRequest;

/**
 * Class FivepostPickupDelivery
 * @package SergeR\Webasyst\AxilogSDK\Integration\Request
 */
class FivepostPickupDelivery extends AbstractDeliveryRequest implements DeliveryRequest
{
    /**
     * FivepostPickupDelivery constructor.
     * @param FivepostDeliveryOrder $order
     */
    public function __construct(FivepostDeliveryOrder $order)
    {
        $this->setOrder($order);
    }

    /**
     * @param FivepostDeliveryOrder $order
     * @return $this
     */
    public function setOrder(FivepostDeliveryOrder $order)
    {
        $this->order = clone $order;

        return $this;
    }

    /**
     * @inheritDoc
     */
    protected function _getMode(): string
    {
        return $this->getOrder()->getOkey() ? 'update_5post_pickup' : 'new_5post_pickup';
    }

    /**
     * @param \SimpleXMLElement $result
     * @return FivepostPickupDeliveryResponse
     */
    protected function _decodeResponse(\SimpleXMLElement $result): FivepostPickupDeliveryResponse
    {
        return new FivepostPickupDeliveryResponse($result);
    }
}
