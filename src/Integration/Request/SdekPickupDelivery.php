<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Request;

use SergeR\Webasyst\AxilogSDK\Integration\Entity\SdekPickupDeliveryOrder;
use SergeR\Webasyst\AxilogSDK\Integration\Response\SdekPickupDeliveryResponse;
use SergeR\Webasyst\AxilogSDK\Interfaces\DeliveryRequest;
use SimpleXMLElement;

/**
 * Class SdekPickupDelivery
 * @package SergeR\Webasyst\AxilogSDK\Integration\Request
 */
class SdekPickupDelivery extends AbstractDeliveryRequest implements DeliveryRequest
{
    /**
     * SdekPickupDelivery constructor.
     * @param SdekPickupDeliveryOrder $order
     */
    public function __construct(SdekPickupDeliveryOrder $order)
    {
        $this->setOrder($order);
    }

    /**
     * @param SdekPickupDeliveryOrder $order
     * @return $this
     */
    public function setOrder(SdekPickupDeliveryOrder $order)
    {
        $this->order = clone $order;
        return $this;
    }

    /**
     * @inheritDoc
     */
    protected function _getMode(): string
    {
        return $this->getOrder()->getOkey() ? 'update_sdek_pickup' : 'new_sdek_pickup';
    }

    /**
     * @param SimpleXMLElement $result
     * @return SdekPickupDeliveryResponse
     */
    protected function _decodeResponse(SimpleXMLElement $result)
    {
        return new SdekPickupDeliveryResponse($result);
    }
}
