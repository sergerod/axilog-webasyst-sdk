<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2021
 * @license Webasyst
 */
declare(strict_types=1);

namespace SergeR\Webasyst\AxilogSDK\Integration\Request;

use SergeR\CakeUtility\Exception\XmlException;
use SergeR\Webasyst\AxilogSDK\Integration\AbstractRequest;
use SergeR\Webasyst\AxilogSDK\Integration\Client;
use SergeR\Webasyst\AxilogSDK\Integration\Response\StatusListResponse;

/**
 * Class StatusListRequest
 * @package SergeR\Webasyst\AxilogSDK\Integration\Request
 */
class StatusListRequest extends AbstractRequest
{
    /** @var string[] */
    protected array $_okeys;

    /**
     * StatusListRequest constructor.
     * @param string[] $_okeys
     */
    public function __construct(string ...$_okeys)
    {
        $this->_okeys = $_okeys;
    }

    /**
     * @param Client $client
     * @return StatusListResponse
     * @throws XmlException
     * @throws \waException
     */
    public function send(Client $client): StatusListResponse
    {
        $this->setAuthRequired($client->getConfig()->isTestMode());
        return parent::send($client);
    }

    /**
     * @return bool
     */
    public function isAuthRequired(): bool
    {
        return $this->_auth_required;
    }

    /**
     * @inheritDoc
     */
    protected function _getMode(): string
    {
        return 'status_list';
    }

    /**
     * @param \SimpleXMLElement $result
     * @return StatusListResponse
     */
    protected function _decodeResponse(\SimpleXMLElement $result): StatusListResponse
    {
        return new StatusListResponse($result);
    }

    protected function _build(): array
    {
        return ['okeylist' => ['okey' => $this->_okeys]];
    }
}
