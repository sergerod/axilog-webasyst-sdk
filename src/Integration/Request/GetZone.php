<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020-2021
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Request;

use SergeR\Webasyst\AxilogSDK\Integration\AbstractRequest;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\AddressZone;
use SergeR\Webasyst\AxilogSDK\Integration\Response\AddressZones;
use SimpleXMLElement;

/**
 * Class GetZone
 * @package SergeR\Webasyst\AxilogSDK\Integration\Request
 */
class GetZone extends AbstractRequest
{
    /** @var AddressZone[] */
    protected array $zones = [];

    /**
     * @param AddressZone ...$zones
     * @return $this
     */
    public function addZone(AddressZone ...$zones)
    {
        foreach ($zones as $zone) {
            $this->zones[] = $zone;
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    protected function _getMode(): string
    {
        return 'get_zone';
    }

    /**
     * @param SimpleXMLElement $result
     * @return AddressZones
     */
    protected function _decodeResponse(SimpleXMLElement $result)
    {
        return new AddressZones($result);
    }

    /**
     * @return array
     */
    protected function _build(): array
    {
        $zone = [];
        foreach ($this->zones as $item) {
            $zone[] = ['@lat' => $item->getLat(), '@long' => $item->getLong()];
        }

        return ['zones' => ['zone' => $zone]];
    }
}
