<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Request;

use SergeR\Webasyst\AxilogSDK\Integration\Entity\SdekPickupDeliveryOrder;

/**
 * Class CalculateSdekPickupDelivery
 * @package SergeR\Webasyst\AxilogSDK\Integration\Request
 */
class CalculateSdekPickupDelivery extends AbstractCalculationRequest
{
    /**
     * @inheritDoc
     */
    protected function _getMode(): string
    {
        return 'get_tarif_new_sdek_pickup';
    }

    /**
     * @param SdekPickupDeliveryOrder $order
     * @return $this
     */
    public function setOrder(SdekPickupDeliveryOrder $order)
    {
        $this->order = $order;
        return $this;
    }
}
