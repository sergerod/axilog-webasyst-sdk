<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Request;

use SergeR\Webasyst\AxilogSDK\Integration\AbstractRequest;
use SergeR\Webasyst\AxilogSDK\Integration\Client;
use SergeR\Webasyst\AxilogSDK\Integration\Response\CalculatedTariffs;
use SergeR\Webasyst\AxilogSDK\Interfaces\DeliveryOrder;
use SimpleXMLElement;

/**
 * Class AbstractCalculationRequest
 * @package SergeR\Webasyst\AxilogSDK\Integration\Request
 * @method CalculatedTariffs send(Client $client)
 */
abstract class AbstractCalculationRequest extends AbstractRequest
{
    /** @var DeliveryOrder */
    protected DeliveryOrder $order;

    /**
     * @return DeliveryOrder
     */
    public function getOrder(): DeliveryOrder
    {
        return $this->order;
    }

    /**
     * @return array
     */
    protected function _build(): array
    {
        return ['order' => $this->getOrder()->toArray()];
    }

    protected function _decodeResponse(SimpleXMLElement $result): CalculatedTariffs
    {
        return new CalculatedTariffs($result);
    }
}
