<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Request;

use SergeR\Webasyst\AxilogSDK\Integration\Entity\PostalDeliveryOrder;

/**
 * Class CalculatePostalDelivery
 * @package SergeR\Webasyst\AxilogSDK\Integration\Request
 *
 * @method getOrder() : PostalDeliveryOrder
 */
class CalculatePostalDelivery extends AbstractCalculationRequest
{
    /**
     * @inheritDoc
     */
    protected function _getMode(): string
    {
        return 'get_tarif_new_post';
    }

    /**
     * @param PostalDeliveryOrder $order
     * @return $this
     */
    public function setOrder(PostalDeliveryOrder $order)
    {
        $this->order = $order;
        return $this;
    }
}
