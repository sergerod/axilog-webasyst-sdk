<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020-2021
 * @license
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Request;

use SergeR\Webasyst\AxilogSDK\Integration\AbstractRequest;
use SergeR\Webasyst\AxilogSDK\Integration\Client;
use SergeR\Webasyst\AxilogSDK\Integration\Response\DeleteResponse;

/**
 * Class DeleteRequest
 * @package SergeR\Webasyst\AxilogSDK\Integration\Request
 * @method DeleteResponse send(Client $client)
 */
class DeleteRequest extends AbstractRequest
{
    /** @var string */
    protected $object_key;

    /**
     * DeleteRequest constructor.
     * @param $object_key
     */
    public function __construct($object_key)
    {
        $this->object_key = $object_key;
    }

    /**
     * @inheritDoc
     */
    protected function _getMode(): string
    {
        return 'delete';
    }

    protected function _build(): array
    {
        return ['okey' => $this->object_key];
    }

    protected function _decodeResponse(\SimpleXMLElement $result): DeleteResponse
    {
        return new DeleteResponse($result);
    }
}
