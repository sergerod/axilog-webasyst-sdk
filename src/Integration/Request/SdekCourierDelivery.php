<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Request;

use SergeR\Webasyst\AxilogSDK\Integration\Entity\SdekCourierDeliveryOrder;
use SergeR\Webasyst\AxilogSDK\Integration\Response\SdekCourierDeliveryResponse;
use SergeR\Webasyst\AxilogSDK\Interfaces\DeliveryRequest;
use SimpleXMLElement;

/**
 * Class SdekCourierDelivery
 * @package SergeR\Webasyst\AxilogSDK\Integration\Request
 */
class SdekCourierDelivery extends AbstractDeliveryRequest implements DeliveryRequest
{
    /**
     * SdekCourierDelivery constructor.
     * @param SdekCourierDeliveryOrder $order
     */
    public function __construct(SdekCourierDeliveryOrder $order)
    {
        $this->setOrder($order);
    }

    /**
     * @param SdekCourierDeliveryOrder $order
     * @return $this
     */
    public function setOrder(SdekCourierDeliveryOrder $order)
    {
        $this->order = clone $order;
        return $this;
    }

    /**
     * @inheritDoc
     */
    protected function _getMode(): string
    {
        return $this->getOrder()->getOkey() ? 'update_sdek_courier' : 'new_sdek_courier';
    }

    /**
     * @param SimpleXMLElement $result
     * @return SdekCourierDeliveryResponse
     */
    protected function _decodeResponse(SimpleXMLElement $result)
    {
        return new SdekCourierDeliveryResponse($result);
    }
}
