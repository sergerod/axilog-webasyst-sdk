<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020-2021
 * @license
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Request;

use SergeR\CakeUtility\Exception\XmlException;
use SergeR\Webasyst\AxilogSDK\Integration\AbstractRequest;
use SergeR\Webasyst\AxilogSDK\Integration\Client;
use SergeR\Webasyst\AxilogSDK\Integration\Response\StatusResponse;

/**
 * Class StatusRequest
 * @package SergeR\Webasyst\AxilogSDK\Integration\Request
 */
class StatusRequest extends AbstractRequest
{
    /** @var string */
    protected string $object_key;

    /**
     * StatusRequest constructor.
     * @param $object_key
     */
    public function __construct(string $object_key)
    {
        $this->object_key = $object_key;
    }

    /**
     * @param Client $client
     * @return mixed
     * @throws \waException
     * @throws XmlException
     */
    public function send(Client $client)
    {
        $this->setAuthRequired($client->getConfig()->isTestMode());
        return parent::send($client);
    }

    /**
     * @inheritDoc
     */
    protected function _getMode(): string
    {
        return 'status';
    }

    /**
     * @param \SimpleXMLElement $result
     * @return StatusResponse
     */
    protected function _decodeResponse(\SimpleXMLElement $result)
    {
        return new StatusResponse($result);
    }

    /**
     * @return array
     */
    protected function _build(): array
    {
        return ['okey' => $this->object_key];
    }
}
