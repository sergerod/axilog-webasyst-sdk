<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Request;

use SergeR\Webasyst\AxilogSDK\Integration\AbstractRequest;
use SergeR\Webasyst\AxilogSDK\Integration\Client;
use SergeR\Webasyst\AxilogSDK\Integration\Response\FivePostPickupList;

/**
 * Class Get5postPickup
 * @package SergeR\Webasyst\AxilogSDK\Integration\Request
 * @method FivePostPickupList send(Client $client)
 */
class Get5postPickup extends AbstractRequest
{

    /**
     * @inheritDoc
     */
    protected function _getMode(): string
    {
        return 'get_5post_pickup';
    }

    /**
     * @param \SimpleXMLElement $result
     * @return FivePostPickupList
     */
    protected function _decodeResponse(\SimpleXMLElement $result): FivePostPickupList
    {
        return new FivePostPickupList($result);
    }
}
