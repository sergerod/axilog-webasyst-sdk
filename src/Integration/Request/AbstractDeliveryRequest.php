<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license Webasyst
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Request;

use SergeR\Webasyst\AxilogSDK\Integration\AbstractRequest;
use SergeR\Webasyst\AxilogSDK\Integration\Client;
use SergeR\Webasyst\AxilogSDK\Interfaces\DeliveryOrder;
use SergeR\Webasyst\AxilogSDK\Interfaces\DeliveryRequest;
use SergeR\Webasyst\AxilogSDK\Interfaces\DeliveryResponseInterface;

/**
 * Class AbstractDeliveryRequest
 * @package SergeR\Webasyst\AxilogSDK\Integration\Request
 *
 * @method DeliveryResponseInterface send(Client $client)
 */
abstract class AbstractDeliveryRequest extends AbstractRequest implements DeliveryRequest
{
    /** @var DeliveryOrder */
    protected DeliveryOrder $order;

    /**
     * @return DeliveryOrder
     */
    public function getOrder(): DeliveryOrder
    {
        return $this->order;
    }

    protected function _build(): array
    {
        return ['order' => $this->getOrder()->toArray()];
    }
}
