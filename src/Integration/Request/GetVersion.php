<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2021
 * @license MIT
 */
declare(strict_types=1);

namespace SergeR\Webasyst\AxilogSDK\Integration\Request;
use SergeR\Webasyst\AxilogSDK\Integration\AbstractRequest;
use SergeR\Webasyst\AxilogSDK\Integration\Response\GetVersion as GetVersionResponse;

/**
 * Class GetVersion
 * @package SergeR\Webasyst\AxilogSDK\Integration\Request
 */
class GetVersion extends AbstractRequest
{
    public function __construct()
    {
        $this->setAuthRequired(false);
    }

    /**
     * @inheritDoc
     */
    protected function _getMode(): string
    {
        return 'get_version';
    }

    /**
     * @param \SimpleXMLElement $result
     * @return GetVersionResponse
     */
    protected function _decodeResponse(\SimpleXMLElement $result)
    {
        return new GetVersionResponse($result);
    }
}
