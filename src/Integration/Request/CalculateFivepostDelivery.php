<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Request;

use SergeR\Webasyst\AxilogSDK\Integration\Entity\FivepostDeliveryOrder;

/**
 * Class CalculateFivepostDelivery
 * @package SergeR\Webasyst\AxilogSDK\Integration\Request
 */
class CalculateFivepostDelivery extends AbstractCalculationRequest
{
    /**
     * @inheritDoc
     */
    protected function _getMode(): string
    {
        return 'get_tarif_new_5post_pickup';
    }

    /**
     * @param FivepostDeliveryOrder $order
     * @return $this
     */
    public function setOrder(FivepostDeliveryOrder $order)
    {
        $this->order = $order;
        return $this;
    }
}
