<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Request;

use SergeR\Webasyst\AxilogSDK\Integration\AbstractRequest;
use SergeR\Webasyst\AxilogSDK\Integration\Client;
use SergeR\Webasyst\AxilogSDK\Integration\Response\SdekPickupList;
use SimpleXMLElement;

/**
 * Class GetSdekPickup
 * @package SergeR\Webasyst\AxilogSDK\Integration\Request
 * @method SdekPickupList send(Client $client)
 */
class GetSdekPickup extends AbstractRequest
{
    /**
     * @inheritDoc
     */
    protected function _getMode(): string
    {
        return 'get_sdek_pickup';
    }

    protected function _decodeResponse(SimpleXMLElement $result)
    {
        return new SdekPickupList($result);
    }
}
