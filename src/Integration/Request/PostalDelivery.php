<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Request;

use SergeR\Webasyst\AxilogSDK\Integration\Entity\PostalDeliveryOrder;
use SergeR\Webasyst\AxilogSDK\Integration\Response\PostalDelivery as PostalDeliveryResponse;
use SergeR\Webasyst\AxilogSDK\Interfaces\DeliveryRequest;
use SimpleXMLElement;

/**
 * Class PostalDelivery
 * @package SergeR\Webasyst\AxilogSDK\Integration\Request
 */
class PostalDelivery extends AbstractDeliveryRequest implements DeliveryRequest
{
    /**
     * PostalDelivery constructor.
     * @param PostalDeliveryOrder $order
     */
    public function __construct(PostalDeliveryOrder $order)
    {
        $this->setOrder($order);
    }

    /**
     * @param PostalDeliveryOrder $order
     */
    public function setOrder(PostalDeliveryOrder $order): void
    {
        $this->order = clone $order;
    }

    /**
     * @inheritDoc
     */
    protected function _getMode(): string
    {
        return $this->getOrder()->getOkey() ? 'update_post' : 'new_post';
    }

    /**
     * @param SimpleXMLElement $result
     * @return PostalDeliveryResponse
     */
    protected function _decodeResponse(SimpleXMLElement $result)
    {
        return new PostalDeliveryResponse($result);
    }
}
