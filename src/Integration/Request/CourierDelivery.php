<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Request;

use SergeR\Webasyst\AxilogSDK\Integration\Entity\CourierDeliveryOrder;
use SergeR\Webasyst\AxilogSDK\Integration\Response;
use SergeR\Webasyst\AxilogSDK\Interfaces\DeliveryRequest;

/**
 * Class CourierDelivery
 * @package SergeR\Webasyst\AxilogSDK\Integration\Request
 */
class CourierDelivery extends AbstractDeliveryRequest implements DeliveryRequest
{
    /** @var CourierDeliveryOrder */
    protected \SergeR\Webasyst\AxilogSDK\Interfaces\DeliveryOrder $order;

    public function __construct(CourierDeliveryOrder $order)
    {
        $this->setOrder($order);
    }

    /**
     * @param CourierDeliveryOrder $order
     * @return CourierDelivery
     */
    public function setOrder(CourierDeliveryOrder $order)
    {
        $this->order = clone $order;
        return $this;
    }

    /**
     * @inheritDoc
     */
    protected function _getMode(): string
    {
        return $this->getOrder()->getOkey() ? 'update' : 'new';
    }

    protected function _decodeResponse(\SimpleXMLElement $result): Response\CourierDeliveryResponse
    {
        return new Response\CourierDeliveryResponse($result);
    }
}
