<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020-2021
 * @license
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Request;

use SergeR\Webasyst\AxilogSDK\Integration\AbstractRequest;
use SergeR\Webasyst\AxilogSDK\Integration\Response\GetLabelResponse;
use SimpleXMLElement;

/**
 * Class GetLabelRequest
 * @package SergeR\Webasyst\AxilogSDK\Integration\Request
 */
class GetLabelRequest extends AbstractRequest
{
    /** @var string[] */
    protected array $hash = [];

    /**
     * GetLabelRequest constructor.
     * @param string[] $hash
     */
    public function __construct(string ...$hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return string[]
     */
    public function getHash(): array
    {
        return $this->hash;
    }

    /**
     * @param string[] $hash
     * @return $this
     */
    public function setHash(array $hash)
    {
        $this->hash = $hash;
        return $this;
    }

    /**
     * @param $hash
     * @return $this
     */
    public function addHash(string $hash)
    {
        $this->hash[] = $hash;
        return $this;
    }

    /**
     * @inheritDoc
     */
    protected function _getMode(): string
    {
        return 'get_label';
    }

    /**
     * @param SimpleXMLElement $result
     * @return GetLabelResponse
     */
    protected function _decodeResponse(SimpleXMLElement $result)
    {
        return new GetLabelResponse($result);
    }

    protected function _build(): array
    {
        return ['orders' => ['hash' => $this->getHash()]];
    }
}
