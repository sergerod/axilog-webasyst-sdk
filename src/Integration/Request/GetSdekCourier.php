<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Request;

use SergeR\Webasyst\AxilogSDK\Integration\AbstractRequest;
use SergeR\Webasyst\AxilogSDK\Integration\Client;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\SdekCourierDeliveryCity;
use SergeR\Webasyst\AxilogSDK\Integration\Response\SdekCourierCities;
use SimpleXMLElement;

/**
 * Class getSdekCourier
 * @package SergeR\Webasyst\AxilogSDK\Integration\Request
 * @method SdekCourierCities send(Client $client)
 */
class GetSdekCourier extends AbstractRequest
{

    /**
     * @inheritDoc
     */
    protected function _getMode(): string
    {
        return 'get_sdek_courier';
    }

    protected function _decodeResponse(SimpleXMLElement $result)
    {
        return new SdekCourierCities($result);
    }
}
