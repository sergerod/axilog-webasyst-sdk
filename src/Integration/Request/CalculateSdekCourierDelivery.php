<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Request;

use SergeR\Webasyst\AxilogSDK\Integration\Entity\SdekCourierDeliveryOrder;

/**
 * Class CalculateSdekCourierDelivery
 * @package SergeR\Webasyst\AxilogSDK\Integration\Request
 */
class CalculateSdekCourierDelivery extends AbstractCalculationRequest
{
    /**
     * @inheritDoc
     */
    protected function _getMode(): string
    {
        return 'get_tarif_new_sdek_courier';
    }

    /**
     * @param SdekCourierDeliveryOrder $order
     * @return $this
     */
    public function setOrder(SdekCourierDeliveryOrder $order)
    {
        $this->order = $order;
        return $this;
    }
}
