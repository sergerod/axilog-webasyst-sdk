<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020-2021
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration;

use SergeR\CakeUtility\Xml;
use SergeR\Webasyst\AxilogSDK\Config;

/**
 * Class Client
 * @package SergeR\Webasyst\AxilogSDK\Integration
 */
class Client
{
    protected \SergeR\Webasyst\AxilogSDK\Config $_config;

    public function __construct(Config $config)
    {
        $this->_config = clone $config;
    }

    /**
     * @return Config
     */
    public function getConfig(): Config
    {
        return $this->_config;
    }

    /**
     * @param string $mode
     * @param array $data
     * @param bool $auth
     * @param string $root
     * @return \SimpleXMLElement
     * @throws \SergeR\CakeUtility\Exception\XmlException
     * @throws \waException
     */
    public function send(string $mode, array $data = [], bool $auth = true, string $root = 'singleorder'): \SimpleXMLElement
    {
        $request = $this->buildRequest($mode, $data, $auth, $root);
        $url = $this->getUrl();

        if ($logger = $this->getConfig()->getLogger()) {
            $logger->debug("Отправлен запрос на URL: $url\n" . $request->asXML());
        }

        $net = $this->getConfig()->getNet();

        $response = $net->setOptions(['format' => \waNet::FORMAT_XML, 'request_format' => \waNet::FORMAT_RAW])
            ->query($url, ['data' => $request->asXML()], \waNet::METHOD_POST);

        if ($logger) {
            $logger->debug(
                "От сервера Axilog получен ответ:\nКод HTTP: " .
                $net->getResponseHeader('http_code') . "\n" .
                "Заголовки HTTP:\n" . $this->formatResponseHeaders($net->getResponseHeader()) . "\n" .
                "Содержимое:\n" . $response->asXML());
        }

        return $response;
    }

    /**
     * @param string $mode
     * @param array $data
     * @param bool $auth
     * @param string $root
     * @return \SimpleXMLElement
     * @throws \SergeR\CakeUtility\Exception\XmlException
     */
    public function buildRequest(string $mode, array $data = [], bool $auth = true, string $root = 'singleorder'): \SimpleXMLElement
    {
        $request = [$root => ['mode' => [$mode]]];

        if ($auth) {
            $request[$root]['auth'] = ['@ukey' => $this->_config->getUkey()];
        }

        if ($data) {
            unset($data['mode']);
            $request[$root] = array_merge($request[$root], $data);
        }

        return Xml::build($request);
    }

    public function getUrl(): string
    {
        return $this->getConfig()->isTestMode() ? 'https://axilog.ru/atlas/api_xml.php' : 'https://axilog.ru/atlas/api_xml.php';
    }

    /**
     * Форматирует массив key->value заголовков в построчный вывод
     * @param array $headers
     * @return string
     */
    protected function formatResponseHeaders(array $headers): string
    {
        $max_header_length = max(array_map('strlen', array_keys($headers)));
        $max_header_length += 2;
        $flat_array = [];
        foreach ($headers as $key => $value) {
            $flat_array[] = str_pad($key, $max_header_length) . (strlen($value) ? $value : "");
        }

        return implode("\n", $flat_array);
    }
}
