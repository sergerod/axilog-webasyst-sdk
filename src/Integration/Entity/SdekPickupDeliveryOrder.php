<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity;

use SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder\OrderedItems;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder\PlaceBarcodes;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder\SdekServices;

/**
 * Class SdekPickupDeliveryOrder
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity
 */
class SdekPickupDeliveryOrder extends AbstractDeliveryOrder
{
    protected string $d_date = '';

    protected int $post_type = 10;

    protected int $wrap_type = 2;

    protected bool $exact_wrap = false;

    protected $pickup_office_id;

    /** @var SdekServices */
    protected SdekServices $services;

    /**
     * SdekPickupDeliveryOrder constructor.
     */
    public function __construct()
    {
        $this->services = new SdekServices();
        $this->setItems(new OrderedItems);
    }

    /**
     * @return string
     */
    public function getDDate(): string
    {
        return $this->d_date;
    }

    /**
     * @param string $d_date
     * @return $this
     */
    public function setDDate(string $d_date)
    {
        $this->d_date = $d_date;
        return $this;
    }

    /**
     * @return int
     */
    public function getPostType(): int
    {
        return $this->post_type;
    }

    /**
     * @param int $post_type
     * @return $this
     */
    public function setPostType(int $post_type)
    {
        $this->post_type = $post_type;
        return $this;
    }

    /**
     * @return int
     */
    public function getWrapType(): int
    {
        return $this->wrap_type;
    }

    /**
     * @param int $wrap_type
     * @return $this
     */
    public function setWrapType(int $wrap_type)
    {
        $this->wrap_type = $wrap_type;
        return $this;
    }

    /**
     * @return bool
     */
    public function isExactWrap(): bool
    {
        return $this->exact_wrap;
    }

    /**
     * @param bool $exact_wrap
     * @return $this
     */
    public function setExactWrap(bool $exact_wrap)
    {
        $this->exact_wrap = $exact_wrap;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPickupOfficeId()
    {
        return $this->pickup_office_id;
    }

    /**
     * @param mixed $pickup_office_id
     * @return SdekPickupDeliveryOrder
     */
    public function setPickupOfficeId($pickup_office_id)
    {
        $this->pickup_office_id = $pickup_office_id;
        return $this;
    }

    /**
     * @return SdekServices
     */
    public function getServices(): SdekServices
    {
        return $this->services;
    }

    /**
     * @param SdekServices $services
     * @return $this
     */
    public function setServices(SdekServices $services)
    {
        $this->services = clone $services;
        return $this;
    }

    public function toArray(): array
    {
        $data = [];

        if ($this->getInnerId()) $data['@inner_id'] = $this->getInnerId();
        if ($this->getOkey()) $data['@okey'] = $this->getOkey();

        $data['@name'] = $this->getName();
        $data['@d_date'] = $this->getDDate();
        $data['@post_type'] = $this->getPostType();

        if ($this->getWrapType() !== null) {
            $data['@wrap_type'] = $this->getWrapType();
        }

        $data['@exact_wrap'] = $this->isExactWrap() ? 1 : 0;

        if ($this->getInclDelivSum() !== null) {
            $data['@incl_deliv_sum'] = $this->getInclDelivSum();
        }

        if ($this->getEmail()) {
            $data['@email'] = $this->getEmail();
        }

        if ($this->getSite()) {
            $data['@site'] = $this->getSite();
        }

        if ($this->getSms()) {
            $data['@sms'] = $this->getSms();
        }

        $data['address'] = ['@office_code' => $this->getPickupOfficeId()];
        $data['contacts'] = $this->getContacts();
        $data['services'] = $this->getServices()->toArray();

        $items = $this->getItems()->toArray();
        array_walk($items, function (&$v) {
            unset($v['@expmode'], $v['@failprice']);
        });

        $data['items'] = ['item' => $items];

        if ($this->getBarcodes()) {
            $data['barcodes'] = ['barcode' => $this->getBarcodes()];
        }

        return $data;
    }
}
