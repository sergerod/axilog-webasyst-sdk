<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity;

use SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder\OrderedItems;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder\PlaceBarcodes;
use SergeR\Webasyst\AxilogSDK\Interfaces\ArraySerializable;
use SergeR\Webasyst\AxilogSDK\Interfaces\DeliveryOrder;

/**
 * Class AbstractDeliveryOrder
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity
 */
abstract class AbstractDeliveryOrder implements ArraySerializable, DeliveryOrder
{
    /** @var string */
    protected string $inner_id = '';

    /** @var string|null */
    protected ?string $okey = null;

    protected $incl_deliv_sum;

    /** @var string */
    protected string $name = '';

    /** @var string */
    protected string $email = '';

    /** @var string */
    protected string $site = '';

    /** @var string */
    protected string $sms = '';

    /** @var string */
    protected string $contacts = '';

    /** @var OrderedItems */
    protected OrderedItems $items;

    /** @var PlaceBarcodes|null */
    protected ?PlaceBarcodes $barcodes = null;

    /**
     * AbstractDeliveryOrder constructor.
     */
    public function __construct()
    {
        $this->items = new OrderedItems();
    }

    /**
     * @return string
     */
    public function getInnerId(): string
    {
        return $this->inner_id;
    }

    /**
     * @param string $inner_id
     * @return $this
     */
    public function setInnerId(string $inner_id): AbstractDeliveryOrder
    {
        $this->inner_id = $inner_id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOkey()
    {
        return $this->okey;
    }

    /**
     * @param string|null $okey
     * @return $this
     */
    public function setOkey(?string $okey)
    {
        $this->okey = $okey;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInclDelivSum()
    {
        return $this->incl_deliv_sum;
    }

    /**
     * @param mixed $incl_deliv_sum
     * @return $this
     */
    public function setInclDelivSum($incl_deliv_sum)
    {
        $this->incl_deliv_sum = $incl_deliv_sum;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param string $site
     * @return $this
     */
    public function setSite(string $site)
    {
        $this->site = $site;
        return $this;
    }

    /**
     * @return string
     */
    public function getSms()
    {
        return $this->sms;
    }

    /**
     * @param string $sms
     * @return $this
     */
    public function setSms(string $sms)
    {
        $this->sms = $sms;
        return $this;
    }

    /**
     * @return string
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * @param string $contacts
     * @return $this
     */
    public function setContacts(string $contacts)
    {
        $this->contacts = $contacts;
        return $this;
    }

    /**
     * @param OrderedItems $items
     * @return $this
     */
    public function setItems(OrderedItems $items)
    {
        $this->items = clone $items;
        return $this;
    }

    /**
     * @return OrderedItems
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return PlaceBarcodes|null
     */
    public function getBarcodes()
    {
        return $this->barcodes;
    }

    /**
     * @param PlaceBarcodes|null $barcodes
     * @return $this
     */
    public function setBarcodes(?PlaceBarcodes $barcodes)
    {
        $this->barcodes = $barcodes;
        return $this;
    }

    abstract public function toArray(): array;
}
