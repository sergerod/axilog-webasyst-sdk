<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity;

use DateTimeImmutable;

/**
 * Class CalculatedTariff
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity
 */
class CalculatedTariff
{
    /** @var float|null */
    protected ?float $value = null;

    /** @var DateTimeImmutable|null */
    protected ?DateTimeImmutable $delivery_date = null;

    /** @var string|null */
    protected ?string $error = null;

    /** @var string */
    protected string $name = '';

    /** @var int|null */
    protected ?int $post_type = null;

    /**
     * @param float|null $value
     * @return CalculatedTariff
     */
    public function setValue(?float $value): CalculatedTariff
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getValue(): ?float
    {
        return $this->value;
    }

    /**
     * @param DateTimeImmutable|null $delivery_date
     * @return CalculatedTariff
     */
    public function setDeliveryDate(?DateTimeImmutable $delivery_date): CalculatedTariff
    {
        $this->delivery_date = $delivery_date;
        return $this;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getDeliveryDate(): ?DateTimeImmutable
    {
        return $this->delivery_date;
    }

    /**
     * @param string|null $error
     * @return CalculatedTariff
     */
    public function setError(?string $error): CalculatedTariff
    {
        $this->error = $error;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getError(): ?string
    {
        return $this->error;
    }

    /**
     * @param string $name
     * @return CalculatedTariff
     */
    public function setName(string $name): CalculatedTariff
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function isError(): bool
    {
        return (bool) $this->getError();
    }

    /**
     * @param int|null $post_type
     * @return CalculatedTariff
     */
    public function setPostType(?int $post_type): CalculatedTariff
    {
        $this->post_type = $post_type;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPostType(): ?int
    {
        return $this->post_type;
    }
}
