<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder;

use SergeR\Webasyst\AxilogSDK\Interfaces\ArraySerializable;

/**
 * Class DiscountSetItem
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder
 */
class DiscountSetItem implements ArraySerializable
{
    protected float $below_sum = 0.0;
    protected float $discount = 0.0;

    /**
     * @return float
     */
    public function getBelowSum(): float
    {
        return $this->below_sum;
    }

    /**
     * @param float $below_sum
     * @return DiscountSetItem
     */
    public function setBelowSum(float $below_sum): DiscountSetItem
    {
        $this->below_sum = $below_sum;
        return $this;
    }

    /**
     * @return float
     */
    public function getDiscount(): float
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     * @return DiscountSetItem
     */
    public function setDiscount(float $discount): DiscountSetItem
    {
        $this->discount = $discount;
        return $this;
    }

    public function toArray(): array
    {
        return ['below' => [
            '@below_sum' => number_format($this->getBelowSum(), 2, '.', 0),
            '@discount'  => rtrim(rtrim(number_format($this->getDiscount(), 2, '.', ''), '0'), '.')
        ]];
    }
}
