<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder;

use SergeR\Webasyst\AxilogSDK\Interfaces\ArraySerializable;

/**
 * Class SdekStreetAddress
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder
 */
class SdekStreetAddress implements ArraySerializable
{
    /**
     * @var int
     */
    protected int $city_code = 0;
    /**
     * @var string
     */
    protected string $street = '';
    /**
     * @var string
     */
    protected string $house = '';
    /**
     * @var string|null
     */
    protected ?string $apartment = null;

    /**
     * @return int
     */
    public function getCityCode(): int
    {
        return $this->city_code;
    }

    /**
     * @param int $city_code
     * @return SdekStreetAddress
     */
    public function setCityCode(int $city_code): SdekStreetAddress
    {
        $this->city_code = $city_code;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return SdekStreetAddress
     */
    public function setStreet(string $street): SdekStreetAddress
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string
     */
    public function getHouse(): string
    {
        return $this->house;
    }

    /**
     * @param string $house
     * @return SdekStreetAddress
     */
    public function setHouse(string $house): SdekStreetAddress
    {
        $this->house = $house;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getApartment(): ?string
    {
        return $this->apartment;
    }

    /**
     * @param string|null $apartment
     * @return SdekStreetAddress
     */
    public function setApartment(?string $apartment): SdekStreetAddress
    {
        $this->apartment = $apartment;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        $data = [
            '@city_code' => $this->getCityCode(),
            '@street'    => $this->getStreet(),
            '@house'     => $this->getHouse()
        ];
        if ($this->getApartment() !== null) {
            $data['@apartment'] = $this->getApartment();
        }

        return $data;
    }
}
