<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder;

use SergeR\Webasyst\AxilogSDK\Interfaces\ArraySerializable;

/**
 * Class DiscountSet
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder
 */
class DiscountSet implements ArraySerializable, \Countable
{
    protected float $above_discount = 0.0;

    /** @var DiscountSetItem[] */
    protected array $items = [];

    /**
     * @return float
     */
    public function getAboveDiscount(): float
    {
        return $this->above_discount;
    }

    /**
     * @param float $above_discount
     * @return DiscountSet
     */
    public function setAboveDiscount(float $above_discount): DiscountSet
    {
        $this->above_discount = $above_discount;
        return $this;
    }

    /**
     * @param DiscountSetItem $item
     * @return $this
     */
    public function add(DiscountSetItem $item)
    {
        $this->items[] = clone $item;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [
            '@above_discount' => rtrim(rtrim(number_format($this->getAboveDiscount(), 2, '.', ''), '0'), '.')
        ];

        foreach ($this->items as $item) {
            $data['below'][] = $item->toArray()['below'];
        }

        return $data;
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return count($this->items);
    }
}
