<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder;

use SergeR\Webasyst\AxilogSDK\Interfaces\ArraySerializable;

/**
 * Class PostalServices
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder
 */
class PostalServices extends Services implements ArraySerializable
{
    protected ?bool $post_tarif = null;
    protected ?bool $fragile = null;

    /** @var int|null */
    protected $optimize;

    /**
     * @return mixed
     */
    public function isPostTarif(): ?bool
    {
        return $this->post_tarif;
    }

    /**
     * @param mixed $post_tarif
     * @return PostalServices
     */
    public function setPostTarif(?bool $post_tarif): PostalServices
    {
        $this->post_tarif = $post_tarif;
        return $this;
    }

    /**
     * @return mixed
     */
    public function isFragile(): ?bool
    {
        return $this->fragile;
    }

    /**
     * @param bool|null $fragile
     * @return PostalServices
     */
    public function setFragile(?bool $fragile)
    {
        $this->fragile = $fragile;
        return $this;
    }

    /**
     * @return bool|null
     * @deprecated
     */
    public function isOptimize(): ?bool
    {
        return $this->optimize === null ? null : (bool)$this->optimize;
    }

    /**
     * @return int|null
     */
    public function getOptimize(): ?int
    {
        return $this->optimize;
    }

    /**
     * @param int|null $optimize
     * @return PostalServices
     */
    public function setOptimize(?int $optimize): PostalServices
    {
        $this->optimize = $optimize;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        $data = parent::toArray();

        if ($this->isCod()) {
            if ($this->isPostTarif() !== null) {
                $data['@post_tarif'] = $this->isPostTarif() ? 'yes' : 'no';
            }
        }

        if ($this->isFragile() !== null) {
            $data['@fragile'] = $this->isFragile() ? 'yes' : 'no';
        }

        if ($this->getOptimize() !== null) {
            $data['@optimize'] = $this->getOptimize();
        }

        return $data;
    }
}
