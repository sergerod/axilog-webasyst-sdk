<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder;

use SergeR\Webasyst\AxilogSDK\Interfaces\ArraySerializable;

/**
 * Class PlaceBarcodes
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder
 */
class PlaceBarcodes implements ArraySerializable
{
    /** @var PlaceBarcode[] */
    protected array $barcodes = [];

    public function add(PlaceBarcode $barcode): void
    {
        $this->barcodes[] = clone $barcode;
    }

    public function toArray(): array
    {
        $data = [];

        foreach ($this->barcodes as $barcode) {
            $data[] = $barcode->toArray();
        }

        return $data;
    }
}
