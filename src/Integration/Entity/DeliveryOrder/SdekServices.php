<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder;

use SergeR\Webasyst\AxilogSDK\Interfaces\ArraySerializable;

/**
 * Class SdekServices
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder
 */
class SdekServices extends Services implements ArraySerializable
{
    protected int $optimize = 0;

    /**
     * @return bool
     * @deprecated
     */
    public function isOptimize(): bool
    {
        return (bool) $this->optimize;
    }

    /**
     * @return int
     */
    public function getOptimize(): int
    {
        return $this->optimize;
    }

    /**
     * @param int $optimize
     * @return SdekServices
     */
    public function setOptimize(int $optimize): SdekServices
    {
        $this->optimize = $optimize;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = parent::toArray();
        $data['@optimize'] = $this->getOptimize();

        return $data;
    }
}
