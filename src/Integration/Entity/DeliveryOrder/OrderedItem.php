<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder;

use SergeR\Webasyst\AxilogSDK\Interfaces\ArraySerializable;

/**
 * Class OrderedItem
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder
 */
class OrderedItem implements ArraySerializable
{
    protected string $name = '';
    protected float $weight = 0.0;
    protected float $price = 0.0;
    protected int $quantity = 1;
    protected string $article = '';
    protected string $mark = '';
    protected bool $expmode = false;
    protected ?float $failprice = null;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return OrderedItem
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     * @return OrderedItem
     */
    public function setWeight(float $weight)
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return OrderedItem
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return OrderedItem
     */
    public function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return string
     */
    public function getArticle(): string
    {
        return $this->article;
    }

    /**
     * @param string $article
     * @return OrderedItem
     */
    public function setArticle(string $article)
    {
        $this->article = $article;
        return $this;
    }

    /**
     * @return string
     */
    public function getMark(): string
    {
        return $this->mark;
    }

    /**
     * @param string $mark
     * @return OrderedItem
     */
    public function setMark(string $mark)
    {
        $this->mark = $mark;
        return $this;
    }

    /**
     * @return bool
     */
    public function isExpmode(): bool
    {
        return $this->expmode;
    }

    /**
     * @param bool $expmode
     * @return OrderedItem
     */
    public function setExpmode(bool $expmode)
    {
        $this->expmode = $expmode;
        return $this;
    }

    /**
     * @return null|float
     */
    public function getFailprice(): ?float
    {
        return $this->failprice;
    }

    /**
     * @param float|null $failprice
     * @return OrderedItem
     */
    public function setFailprice(?float $failprice)
    {
        $this->failprice = $failprice;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [
            '@name'     => $this->getName(),
            '@weight'   => number_format($this->getWeight(), 3, '.', ''),
            '@quantity' => $this->getQuantity(),
            '@price'    => number_format($this->getPrice(), 2, '.', ''),
        ];

        if ($article = trim($this->getArticle())) {
            $data['@article'] = $article;
        }

        if ($mark = trim($this->getMark())) {
            $data['@mark'] = $mark;
        }

        if ($this->isExpmode()) {
            $data['@expmode'] = '1';
        }

        if ($this->getFailprice() !== null) {
            $data['@failprice'] = number_format($this->getFailprice(), 2, '.', '');
        }

        return $data;
    }
}
