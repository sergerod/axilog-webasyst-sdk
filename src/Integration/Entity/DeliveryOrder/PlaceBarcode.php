<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder;

/**
 * Class PlaceBarcode
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder
 */
class PlaceBarcode
{
    protected int $place = 1;
    protected string $barcode = '';

    /**
     * @return int
     */
    public function getPlace(): int
    {
        return $this->place;
    }

    /**
     * @param int $place
     * @return PlaceBarcode
     */
    public function setPlace(int $place): PlaceBarcode
    {
        $this->place = $place;
        return $this;
    }

    /**
     * @return string
     */
    public function getBarcode(): string
    {
        return $this->barcode;
    }

    /**
     * @param string $barcode
     * @return PlaceBarcode
     */
    public function setBarcode(string $barcode): PlaceBarcode
    {
        $this->barcode = $barcode;
        return $this;
    }

    public function toArray(): array
    {
        return ['@place' => $this->getPlace(), $this->getBarcode()];
    }
}
