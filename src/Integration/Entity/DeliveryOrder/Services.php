<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder;

use SergeR\Webasyst\AxilogSDK\Interfaces\ArraySerializable;

/**
 * Class Services
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder
 */
class Services implements ArraySerializable
{
    /** @var bool|null */
    protected ?bool $valuation = null;

    /** @var bool|null */
    protected ?bool $cod = null;

    /**
     * @param bool $valuation
     * @return $this
     */
    public function setValuation(?bool $valuation): Services
    {
        $this->valuation = $valuation;
        return $this;
    }

    /**
     * @return bool
     */
    public function isValuation(): ?bool
    {
        return $this->valuation;
    }

    /**
     * @param bool $cod
     * @return $this
     */
    public function setCod(?bool $cod)
    {
        $this->cod = $cod;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCod(): ?bool
    {
        return $this->cod;
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return ($this->isCod() === null) &&
            ($this->isValuation() === null);
    }

    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        $data = [];
        if ($this->isValuation() !== null) {
            $data['@valuation'] = $this->isValuation() ? 'yes' : 'no';
        }

        if ($this->isCod() !== null) {
            $data['@cod'] = $this->isCod() ? 'yes' : 'no';
            if ($this->isCod()) {
                $data['@valuation'] = 'yes';
            }
        }

        return $data;
    }
}
