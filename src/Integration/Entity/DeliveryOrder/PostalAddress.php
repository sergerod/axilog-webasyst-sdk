<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder;

use SergeR\Webasyst\AxilogSDK\Interfaces\ArraySerializable;

/**
 * Class PostalAddress
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder
 */
class PostalAddress implements ArraySerializable
{
    protected string $index = '';

    /** @var string */
    protected string $region='';

    /** @var string */
    protected string $area='';

    /** @var string */
    protected string $p_address = '';

    /**
     * @return string
     */
    public function getIndex(): string
    {
        return $this->index;
    }

    /**
     * @param string $index
     * @return PostalAddress
     */
    public function setIndex(string $index)
    {
        $this->index = $index;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegion(): string
    {
        return $this->region;
    }

    /**
     * @param string $region
     * @return PostalAddress
     */
    public function setRegion(string $region): PostalAddress
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @return string
     */
    public function getArea(): string
    {
        return $this->area;
    }

    /**
     * @param string $area
     * @return PostalAddress
     */
    public function setArea(string $area)
    {
        $this->area = $area;
        return $this;
    }

    /**
     * @return string
     */
    public function getPAddress(): string
    {
        return $this->p_address;
    }

    /**
     * @param string $p_address
     * @return PostalAddress
     */
    public function setPAddress(string $p_address)
    {
        $this->p_address = $p_address;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        return array_filter([
            '@index'     => $this->getIndex(),
            '@region'    => $this->getRegion(),
            '@area'      => $this->getArea(),
            '@p_address' => $this->getPAddress()
        ], 'strlen');
    }
}
