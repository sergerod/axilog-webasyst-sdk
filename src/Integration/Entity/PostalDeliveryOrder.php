<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity;

use SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder\PostalAddress;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder\PostalServices;

/**
 * Class PostalDeliveryOrder
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity
 */
class PostalDeliveryOrder extends AbstractDeliveryOrder
{
    /** @var string */
    protected string $b_date = '';

    /** @var int */
    protected int $post_type = 1;

    /** @var null|int */
    protected ?int $wrap_type = null;

    /** @var null|bool */
    protected ?bool $exact_wrap = null;

    /** @var PostalAddress */
    protected PostalAddress $address;

    /** @var PostalServices */
    protected PostalServices $services;

    public function __construct()
    {
        $this->setAddress(new PostalAddress);
        $this->setServices(new PostalServices);
        parent::__construct();
    }

    /**
     * @return string
     */
    public function getBDate(): string
    {
        return $this->b_date;
    }

    /**
     * @param string $b_date
     * @return PostalDeliveryOrder
     */
    public function setBDate(string $b_date): PostalDeliveryOrder
    {
        $this->b_date = $b_date;
        return $this;
    }

    /**
     * @return int
     */
    public function getPostType(): int
    {
        return $this->post_type;
    }

    /**
     * @param int $post_type
     * @return PostalDeliveryOrder
     */
    public function setPostType(int $post_type): PostalDeliveryOrder
    {
        $this->post_type = $post_type;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getWrapType(): ?int
    {
        return $this->wrap_type;
    }

    /**
     * @param int|null $wrap_type
     * @return PostalDeliveryOrder
     */
    public function setWrapType(?int $wrap_type): PostalDeliveryOrder
    {
        $this->wrap_type = $wrap_type;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isExactWrap(): ?bool
    {
        return $this->exact_wrap;
    }

    /**
     * @param bool|null $exact_wrap
     * @return PostalDeliveryOrder
     */
    public function setExactWrap(?bool $exact_wrap): PostalDeliveryOrder
    {
        $this->exact_wrap = $exact_wrap;
        return $this;
    }

    /**
     * @return PostalAddress
     */
    public function getAddress(): PostalAddress
    {
        return $this->address;
    }

    /**
     * @param PostalAddress $address
     * @return PostalDeliveryOrder
     */
    public function setAddress(PostalAddress $address)
    {
        $this->address = clone $address;
        return $this;
    }

    /**
     * @return PostalServices
     */
    public function getServices(): PostalServices
    {
        return $this->services;
    }

    /**
     * @param PostalServices $services
     * @return PostalDeliveryOrder
     */
    public function setServices(PostalServices $services): PostalDeliveryOrder
    {
        $this->services = clone $services;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [];

        if ($this->getInnerId()) {
            $data['@inner_id'] = $this->getInnerId();
        }

        if ($this->getOkey()) {
            $data['@okey'] = $this->getOkey();
        }

        $data['@b_date'] = $this->getBDate();
        $data['@name'] = $this->getName();

        if ($this->getInclDelivSum() !== null) {
            $data['@incl_deliv_sum'] = $this->getInclDelivSum();
        }

        $data['@post_type'] = $this->getPostType();

        if ($this->getEmail()) {
            $data['@email'] = $this->getEmail();
        }

        if ($this->getSms()) {
            $data['@sms'] = $this->getSms();
        }

        if ($this->getSite()) {
            $data['@site'] = $this->getSite();
        }

        if ($this->getWrapType() !== null) {
            $data['@wrap_type'] = $this->getWrapType();
        }

        if ($this->isExactWrap() !== null) {
            $data['@exact_wrap'] = $this->isExactWrap() ? 1 : 0;
        }

        $data['address'] = $this->getAddress()->toArray();
        $data['contacts'] = $this->getContacts();

        if ($this->getServices() && ($services = $this->getServices()->toArray())) {
            $data['services'] = $services;
        }

        $data['items'] = ['item' => array_map(function ($v) {
            unset($v['@expmode'], $v['@failprice']);
            return $v;
        }, $this->getItems()->toArray())];

        if ($this->getBarcodes()) {
            $data['barcodes'] = ['barcode' => $this->getBarcodes()->toArray()];
        }

        return $data;
    }
}
