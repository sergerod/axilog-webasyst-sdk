<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity;

/**
 * Class SdekWeightLimit
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity
 */
class SdekWeightLimit
{
    /** @var null|float */
    protected ?float $min = null;

    /** @var null|float */
    protected ?float $max = null;

    /**
     * @param float|null $min
     * @return $this
     */
    public function setMin(?float $min)
    {
        $this->min = $min;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getMin(): ?float
    {
        return $this->min;
    }

    /**
     * @param float|null $max
     * @return $this
     */
    public function setMax(?float $max)
    {
        $this->max = $max;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getMax(): ?float
    {
        return $this->max;
    }

    /**
     * @param string $limits
     * @return static
     */
    public static function parse(string $limits)
    {
        $instance = new static();
        if (!strlen($limits)) return $instance;

        $limits = explode('-', $limits, 2);
        $limits[0] = isset($limits[0]) && strlen(trim($limits[0])) ? (float)str_replace(',', '.', $limits[0]) : null;
        $limits[1] = isset($limits[1]) && strlen(trim($limits[1])) ? (float)str_replace(',', '.', $limits[1]) : null;

        $instance->setMin(min($limits))->setMax(max($limits));

        return $instance;
    }
}
