<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity;

use SergeR\CakeUtility\Text;

/**
 * Class ResponseStatus
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity
 */
class ResponseStatus
{
    protected ?int $status = null;

    /** @var null|array */
    protected ?array $current_message = null;

    protected ?string $server_message;

    protected array $messages = [
        ['code' => 0, 'message' => 'запрос обработан и выполнен успешно'],
        ['code' => 1, 'message' => 'ошибка идентификации ukey'],
        ['code' => 2, 'message' => 'ошибка контрольной суммы'],
        ['code' => 3, 'message' => 'ошибка в ФИО, адресе, зоне доставки'],
        ['code' => 4, 'message' => 'ошибка в дате и времени доставки'],
        ['code' => 5, 'message' => 'ошибка в контактах'],
        ['code' => 6, 'message' => 'ошибка в доп.услугах'],
        ['code' => 7, 'message' => 'ошибка в товарах'],
        ['code' => 8, 'message' => 'неверный формат xml'],
        ['code' => 9, 'message' => 'не задан auth'],
        ['code' => 10, 'message' => 'неверный auth'],
        ['code' => 11, 'message' => 'неверный fid'],
        ['code' => 12, 'message' => 'заявок по детализации не найдено'],
        ['code' => 13, 'message' => 'не соответствие профилю клиента'],
        ['code' => 14, 'message' => 'ошибка получателя'],
        ['code' => 15, 'message' => 'ошибка sms-номера'],
        ['code' => 16, 'message' => 'ошибка номера телефона'],
        ['code' => 17, 'message' => 'ошибка оформления договора'],
        ['code' => 18, 'message' => 'некорректный индекс'],
        ['code' => 19, 'message' => 'отсутствует Агентский договор'],
        ['code' => 20, 'message' => 'заказ не найден'],
        ['code' => 21, 'message' => 'превышена квота заказов'],
        ['code' => 22, 'message' => 'превышено допустимое количество запросов в секунду'],
        ['code' => 23, 'message' => 'недопустимый запрос'],
        ['code' => 24, 'message' => 'не допускается аннулирование заказа'],
        ['code' => 25, 'message' => 'недопустимый ПВЗ'],
        ['code' => 26, 'message' => 'ошибка процесса выполнения запроса'],
        ['code' => 27, 'message' => 'ошибка параметров дифференцированной стоимости доставки'],
    ];

    /**
     * ResponseStatus constructor.
     * @param int|null $status
     * @param string|null $server_message
     */
    public function __construct(?int $status, ?string $server_message = null)
    {
        $this->status = $status;
        $this->server_message = $server_message;
    }

    /**
     * @return int
     */
    public function getCode(): ?int
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        if ($this->status === null) return 'статус запроса не определён';
        if ($this->current_message === null) $this->_findMessage();
        if (empty($this->current_message)) return Text::insert('неизвестная ошибка (:code)', ['code' => $this->status]);

        return $this->current_message['message'];
    }

    /**
     * @return array
     */
    protected function _findMessage(): array
    {
        $msg = [];
        $code = $this->status === null ? null : (int)$this->status;

        foreach ($this->messages as $message) {
            if ($message['code'] === $code) {
                $msg = $message;
                $this->current_message = $msg;
                if ($this->server_message) $this->current_message['message'] = $this->server_message;
                break;
            }
        }

        return $msg;
    }
}
