<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity;

use SergeR\Webasyst\AxilogSDK\Interfaces\SdekPickupInterface;

/**
 * Class SdekPickup
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity
 */
class SdekPickup implements SdekPickupInterface
{
    /**
     * @var string
     */
    protected string $code = '';
    /**
     * @var string
     */
    protected string $regionName = '';
    /**
     * @var int
     */
    protected int $cityCode = 0;
    /**
     * @var string
     */
    protected string $address = '';
    /**
     * @var string
     */
    protected string $fullAddress = '';
    /**
     * @var string
     */
    protected string $phone = '';
    protected string $workTime = '';
    protected ?float $coordX = null;
    protected ?float $coordY = null;
    /**
     * @var bool
     */
    protected bool $isDressingRoom = false;
    /**
     * @var bool
     */
    protected bool $haveCashless = false;
    /**
     * @var bool
     */
    protected bool $allowedCod = false;
    /**
     * @var string
     */
    protected string $addressComment = '';

    protected \SergeR\Webasyst\AxilogSDK\Integration\Entity\SdekWeightLimit $weightLimit;

    public function __construct()
    {
        $this->weightLimit = new SdekWeightLimit();
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return SdekPickup
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegionName(): string
    {
        return $this->regionName;
    }

    /**
     * @param string $regionName
     * @return SdekPickup
     */
    public function setRegionName(string $regionName)
    {
        $this->regionName = $regionName;
        return $this;
    }

    /**
     * @return int
     */
    public function getCityCode(): int
    {
        return $this->cityCode;
    }

    /**
     * @param int $cityCode
     * @return SdekPickup
     */
    public function setCityCode(int $cityCode)
    {
        $this->cityCode = $cityCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return SdekPickup
     */
    public function setAddress(string $address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullAddress(): string
    {
        return $this->fullAddress;
    }

    /**
     * @param string $fullAddress
     * @return SdekPickup
     */
    public function setFullAddress(string $fullAddress)
    {
        $this->fullAddress = $fullAddress;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return SdekPickup
     */
    public function setPhone(string $phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getWorkTime(): string
    {
        return $this->workTime;
    }

    /**
     * @param string $workTime
     * @return SdekPickup
     */
    public function setWorkTime(string $workTime)
    {
        $this->workTime = $workTime;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCoordX(): ?float
    {
        return $this->coordX;
    }

    /**
     * @param float|null $coordX
     * @return SdekPickup
     */
    public function setCoordX(?float $coordX)
    {
        $this->coordX = $coordX;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCoordY(): ?float
    {
        return $this->coordY;
    }

    /**
     * @param float|null $coordY
     * @return SdekPickup
     */
    public function setCoordY(?float $coordY)
    {
        $this->coordY = $coordY;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDressingRoom(): bool
    {
        return $this->isDressingRoom;
    }

    /**
     * @param bool $isDressingRoom
     * @return SdekPickup
     */
    public function setIsDressingRoom(bool $isDressingRoom)
    {
        $this->isDressingRoom = $isDressingRoom;
        return $this;
    }

    /**
     * @return bool
     */
    public function isHaveCashless(): bool
    {
        return $this->haveCashless;
    }

    /**
     * @param bool $haveCashless
     * @return SdekPickup
     */
    public function setHaveCashless(bool $haveCashless)
    {
        $this->haveCashless = $haveCashless;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAllowedCod(): bool
    {
        return $this->allowedCod;
    }

    /**
     * @param bool $allowedCod
     * @return SdekPickup
     */
    public function setAllowedCod(bool $allowedCod)
    {
        $this->allowedCod = $allowedCod;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddressComment(): string
    {
        return $this->addressComment;
    }

    /**
     * @param string $addressComment
     * @return SdekPickup
     */
    public function setAddressComment(string $addressComment)
    {
        $this->addressComment = $addressComment;
        return $this;
    }

    /**
     * @return SdekWeightLimit
     */
    public function getWeightLimit(): SdekWeightLimit
    {
        return $this->weightLimit;
    }

    /**
     * @param SdekWeightLimit $weightLimit
     * @return SdekPickup
     */
    public function setWeightLimit(SdekWeightLimit $weightLimit)
    {
        $this->weightLimit = $weightLimit;
        return $this;
    }
}
