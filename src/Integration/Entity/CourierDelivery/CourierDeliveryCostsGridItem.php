<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity\CourierDelivery;

/**
 * Class CourierDeliveryCostsGridItem
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity\CourierDelivery
 */
class CourierDeliveryCostsGridItem
{
    protected float $below_sum = 0.0;
    protected float $price = 0.0;

    /**
     * @return float
     */
    public function getBelowSum(): float
    {
        return $this->below_sum;
    }

    /**
     * @param float $below_sum
     * @return CourierDeliveryCostsGridItem
     */
    public function setBelowSum(float $below_sum)
    {
        $this->below_sum = $below_sum;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return CourierDeliveryCostsGridItem
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return ['below' => [
            '@below_sum' => number_format($this->getBelowSum(), 2, '.', ''),
            '@price'     => number_format($this->getPrice(), 2, '.', '')
        ]];
    }
}
