<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity\CourierDelivery;

use SergeR\Webasyst\AxilogSDK\Interfaces\ArraySerializable;

/**
 * Class CourierDeliveryCostsGrid
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity
 */
class CourierDeliveryCostsGrid implements \Countable, ArraySerializable
{
    protected float $return_price = 0.0;
    protected float $above_price = 0.0;

    /** @var CourierDeliveryCostsGridItem[] */
    protected array $delivset = [];

    /**
     * @return float
     */
    public function getReturnPrice(): float
    {
        return $this->return_price;
    }

    /**
     * @param float $return_price
     * @return CourierDeliveryCostsGrid
     */
    public function setReturnPrice(float $return_price)
    {
        $this->return_price = $return_price;
        return $this;
    }

    /**
     * @return float
     */
    public function getAbovePrice(): float
    {
        return $this->above_price;
    }

    /**
     * @param float $above_price
     * @return CourierDeliveryCostsGrid
     */
    public function setAbovePrice(float $above_price)
    {
        $this->above_price = $above_price;
        return $this;
    }

    /**
     * @return CourierDeliveryCostsGridItem[]
     */
    public function getDelivset(): array
    {
        return $this->delivset;
    }

    /**
     * @param CourierDeliveryCostsGridItem[] $delivset
     * @return CourierDeliveryCostsGrid
     */
    public function setDelivset(CourierDeliveryCostsGridItem ...$delivset)
    {
        $this->delivset = $delivset;
        return $this;
    }

    /**
     * @param CourierDeliveryCostsGridItem $item
     */
    public function add(CourierDeliveryCostsGridItem $item): void
    {
        $this->delivset[] = $item;
    }

    public function toArray(): array
    {
        $data = [
            '@return_price' => number_format($this->getReturnPrice(), 2, '.', ''),
            '@above_price'  => number_format($this->getAbovePrice(), 2, '.', '')
        ];

        if ($delivset = $this->getDelivset()) {
            usort($delivset, function ($a, $b) {
                /**
                 * CourierDeliveryCostsGridItem $a
                 * CourierDeliveryCostsGridItem $b
                 */
                $a_below = $a->getBelowSum();
                $b_below = $b->getBelowSum();
                return $a_below <=> $b_below;
            });
            /** @var CourierDeliveryCostsGridItem $item */
            foreach ($delivset as $item) {
                $data['below'][] = $item->toArray()['below'];
            }
        }

        return $data;
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return count($this->delivset);
    }
}
