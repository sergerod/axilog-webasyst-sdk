<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity;

use SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder\Services;

/**
 * Class FivepostDeliveryOrder
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity
 */
class FivepostDeliveryOrder extends AbstractDeliveryOrder
{
    /** @var string */
    protected string $d_date = '';

    /** @var null|int */
    protected ?int $wrap_type = null;

    /** @var null|bool */
    protected ?bool $exact_wrap = null;

    /** @var string */
    protected string $office_code = '';

    protected Services $services;

    /**
     * FivepostDeliveryOrder constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->services = new Services();
    }

    /**
     * @param string $d_date
     * @return $this
     */
    public function setDDate(string $d_date): FivepostDeliveryOrder
    {
        $this->d_date = $d_date;
        return $this;
    }

    /**
     * @return string
     */
    public function getDDate(): string
    {
        return $this->d_date;
    }

    /**
     * @param int|null $wrap_type
     * @return $this
     */
    public function setWrapType(?int $wrap_type): FivepostDeliveryOrder
    {
        $this->wrap_type = $wrap_type;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getWrapType(): ?int
    {
        return $this->wrap_type;
    }

    /**
     * @param bool|null $exact_wrap
     * @return $this
     */
    public function setExactWrap(?bool $exact_wrap): FivepostDeliveryOrder
    {
        $this->exact_wrap = $exact_wrap;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getExactWrap(): ?bool
    {
        return $this->exact_wrap;
    }

    /**
     * @param string $office_code
     * @return $this
     */
    public function setOfficeCode(string $office_code): FivepostDeliveryOrder
    {
        $this->office_code = $office_code;
        return $this;
    }

    /**
     * @return string
     */
    public function getOfficeCode(): string
    {
        return $this->office_code;
    }

    /**
     * @param Services $services
     * @return $this
     */
    public function setServices(Services $services)
    {
        $this->services = $services;
        return $this;
    }

    /**
     * @return Services
     */
    public function getServices(): Services
    {
        return $this->services;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [
            '@inner_id' => $this->getInnerId(),
            '@d_date'   => $this->getDDate(),
            '@name'     => $this->getName(),
            'address'   => ['@office_code' => $this->getOfficeCode()],
            'contacts'  => $this->getContacts(),
        ];
        if ($this->getOkey()) $data['@okey'] = $this->getOkey();
        if ($this->getWrapType() !== null) $data['@wrap_type'] = $this->getWrapType();
        if ($this->getExactWrap() !== null) $data['@exact_wrap'] = $this->getExactWrap() ? 'yes' : 'no';
        if ($this->getInclDelivSum() !== null) $data['@incl_deliv_sum'] = $this->getInclDelivSum();
        if ($this->getEmail()) $data['@email'] = $this->getEmail();
        if ($this->getSite()) $data['@site'] = $this->getSite();
        if ($this->getSms()) $data['@sms'] = $this->getSms();
        if (!$this->getServices()->isEmpty()) $data['services'] = $this->getServices()->toArray();

        $data['items'] = ['item' => array_map(function ($v) {
            unset($v['@expmode'], $v['@failprice']);
            return $v;
        }, $this->getItems()->toArray())];

        if ($this->getBarcodes()) {
            $data['barcodes'] = ['barcode' => $this->getBarcodes()->toArray()];
        }

        return $data;
    }
}
