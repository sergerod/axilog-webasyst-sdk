<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity;

use SergeR\Webasyst\AxilogSDK\Integration\Entity\CourierDelivery\CourierDeliveryCostsGrid;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder\DiscountSet;
use SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder\OrderedItems;
use SergeR\Webasyst\AxilogSDK\Interfaces\ArraySerializable;
use SergeR\Webasyst\AxilogSDK\Interfaces\DeliveryOrder;

/**
 * Class CourierDeliveryOrder
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity
 */
class CourierDeliveryOrder extends AbstractDeliveryOrder
{
    const CITY_MSK = 0;
    const CITY_SPB = 1;

    protected string $address = '';
    protected int $city = 0;
    protected int $address_zone = 1;
    protected string $d_date = '';
    protected string $b_time = '';
    protected string $e_time = '';
    protected int $places = 1;

    /** @var null|string */
    protected ?string $discount_value = null;

    /** @var int|null */
    protected ?int $discount_unit = null;
    protected bool $avoid_part_return = true;
    protected string $description = '';

    /** @var bool|null */
    protected ?bool $service_cash = null;

    /** @var bool|null */
    protected ?bool $service_cheque = null;

    /** @var bool|null */
    protected ?bool $service_card = null;

    /** @var bool|null */
    protected ?bool $service_fastpayment = null;

    /** @var CourierDeliveryCostsGrid */
    protected CourierDeliveryCostsGrid $delivset;

    /** @var DiscountSet|null */
    protected ?DiscountSet $discountset = null;

    /**
     * CourierDeliveryOrder constructor.
     */
    public function __construct()
    {
        $this->setItems(new OrderedItems());
    }

    /**
     * @param bool|null $service_cash
     * @return CourierDeliveryOrder
     */
    public function setServiceCash(?bool $service_cash)
    {
        $this->service_cash = $service_cash;
        return $this;
    }

    /**
     * @param bool|null $service_cheque
     * @return CourierDeliveryOrder
     */
    public function setServiceCheque(?bool $service_cheque)
    {
        $this->service_cheque = $service_cheque;
        return $this;
    }

    /**
     * @param bool|null $service_card
     * @return CourierDeliveryOrder
     */
    public function setServiceCard(?bool $service_card)
    {
        $this->service_card = $service_card;
        return $this;
    }

    /**
     * @param CourierDeliveryCostsGrid $delivset
     * @return CourierDeliveryOrder
     */
    public function setDelivset(\SergeR\Webasyst\AxilogSDK\Integration\Entity\CourierDelivery\CourierDeliveryCostsGrid $delivset)
    {
        $this->delivset = $delivset;
        return $this;
    }

    /**
     * @param DiscountSet|null $discountset
     * @return CourierDeliveryOrder
     */
    public function setDiscountset(?\SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder\DiscountSet $discountset)
    {
        $this->discountset = $discountset;
        return $this;
    }

    /**
     * @param bool|null $service_fastpayment
     * @return CourierDeliveryOrder
     */
    public function setServiceFastpayment(?bool $service_fastpayment): CourierDeliveryOrder
    {
        $this->service_fastpayment = $service_fastpayment;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [];
        if ($this->getInnerId()) $data['@inner_id'] = $this->getInnerId();
        $data += ['@name' => $this->getName(), '@address' => $this->getAddress()];
        if ($this->getAddressZone()) $data['@address_zone'] = $this->getAddressZone();
        $data['@city'] = $this->getCity();
        $data += [
            '@d_date' => $this->getDDate(),
            '@b_time' => $this->getBTime(),
            '@e_time' => $this->getETime(),
            '@places' => $this->getPlaces()
        ];
        if ($this->getInclDelivSum() !== null) $data['@incl_deliv_sum'] = $this->getInclDelivSum();
        if ($this->getSms()) $data['@sms'] = $this->getSms();
        if ($this->getEmail()) $data['@email'] = $this->getEmail();

        if (null !== $this->getDiscountValue()) {
            $data['@discount_value'] = $this->getDiscountValue();
            if (null !== $this->getDiscountUnit()) $data['@discount_unit'] = $this->getDiscountUnit();
        }

        $data['@avoid_part_return'] = $this->isAvoidPartReturn() ? '1' : '0';
        if ($this->getSite()) {
            $data['@site'] = $this->getSite();
        }

        $data['contacts'] = $this->getContacts();

        if ($this->getDescription()) {
            $data['description'] = $this->getDescription();
        }

        if ($this->isServiceCash() !== null)
            $data['services']['@cash'] = $this->isServiceCash() ? 'yes' : 'no';

        if ($this->isServiceCard() !== null)
            $data['services']['@card'] = $this->isServiceCard() ? 'yes' : 'no';

        if ($this->isServiceCheque() !== null)
            $data['services']['@cheque'] = $this->isServiceCheque() ? 'yes' : 'no';

        if (($this->getCity() === 0) && ($this->isServiceCash() || $this->isServiceCheque()) && !is_null($this->isServiceFastpayment()))
            $data['services']['@fastpayment'] = $this->isServiceFastpayment() ? 'yes' : 'no';

        $data['items'] = ['item' => $this->items->toArray()];
        if ($this->delivset) {
            $data['delivset'] = $this->delivset->toArray();
        }

        if ($this->discountset) {
            $data['discountset'] = $this->discountset->toArray();
        }

        if ($this->barcodes) {
            $data['barcodes'] = ['barcode' => $this->barcodes->toArray()];
        }

        return $data;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return CourierDeliveryOrder
     */
    public function setAddress(string $address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return int
     */
    public function getAddressZone(): int
    {
        return $this->address_zone;
    }

    /**
     * @param int $address_zone
     * @return $this
     */
    public function setAddressZone(int $address_zone)
    {
        $this->address_zone = $address_zone;
        return $this;
    }

    /**
     * @return int
     */
    public function getCity(): int
    {
        return $this->city;
    }

    /**
     * @param int $city
     * @return $this
     */
    public function setCity(int $city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getDDate(): string
    {
        return $this->d_date;
    }

    /**
     * @param string $d_date
     * @return CourierDeliveryOrder
     */
    public function setDDate(string $d_date)
    {
        $this->d_date = $d_date;
        return $this;
    }

    /**
     * @return string
     */
    public function getBTime(): string
    {
        return $this->b_time;
    }

    /**
     * @param string $b_time
     * @return $this
     */
    public function setBTime(string $b_time)
    {
        $this->b_time = $b_time;
        return $this;
    }

    /**
     * @return string
     */
    public function getETime(): string
    {
        return $this->e_time;
    }

    /**
     * @param string $e_time
     * @return $this
     */
    public function setETime(string $e_time)
    {
        $this->e_time = $e_time;
        return $this;
    }

    /**
     * @return int
     */
    public function getPlaces(): int
    {
        return $this->places;
    }

    /**
     * @param int $places
     * @return CourierDeliveryOrder
     */
    public function setPlaces(int $places)
    {
        $this->places = $places;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDiscountValue(): ?string
    {
        return $this->discount_value;
    }

    /**
     * @param string|null $discount_value
     * @return CourierDeliveryOrder
     */
    public function setDiscountValue(?string $discount_value)
    {
        $this->discount_value = $discount_value;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getDiscountUnit(): ?int
    {
        return $this->discount_unit;
    }

    /**
     * @param int|null $discount_unit
     * @return CourierDeliveryOrder
     */
    public function setDiscountUnit(?int $discount_unit)
    {
        $this->discount_unit = $discount_unit;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAvoidPartReturn(): bool
    {
        return $this->avoid_part_return;
    }

    /**
     * @param bool $avoid_part_return
     * @return CourierDeliveryOrder
     */
    public function setAvoidPartReturn(bool $avoid_part_return)
    {
        $this->avoid_part_return = $avoid_part_return;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return CourierDeliveryOrder
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isServiceCash(): ?bool
    {
        return (bool) $this->service_cash;
    }

    /**
     * @return bool|null
     */
    public function isServiceCard(): ?bool
    {
        return $this->service_card;
    }

    /**
     * @return bool|null
     */
    public function isServiceCheque(): ?bool
    {
        return $this->service_cheque;
    }

    /**
     * @return bool|null
     */
    public function isServiceFastpayment(): ?bool
    {
        return $this->service_fastpayment;
    }
}
