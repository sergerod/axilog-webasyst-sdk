<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity;

/**
 * Class FivePostCellLimits
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity
 */
class FivePostCellLimits
{
    /** @var null|int */
    protected ?int $width = null;

    /** @var null|int */
    protected ?int $height = null;

    /** @var null|int */
    protected ?int $length = null;

    /** @var null|int */
    protected ?int $weight = null;

    public function __construct(string $limit)
    {
        $limits = explode('/', $limit);
        if (isset($limits[0])) $this->width = (int)$limits[0];
        if (isset($limits[1])) $this->height = (int)$limits[1];
        if (isset($limits[2])) $this->length = (int)$limits[2];
        if (isset($limits[3])) $this->weight = (int)$limits[3];
    }

    /**
     * @return int|null
     */
    public function getWidth(): ?int
    {
        return $this->width;
    }

    /**
     * @return int|null
     */
    public function getHeight(): ?int
    {
        return $this->height;
    }

    /**
     * @return int|null
     */
    public function getLength(): ?int
    {
        return $this->length;
    }

    /**
     * @return int|null
     */
    public function getWeight(): ?int
    {
        return $this->weight;
    }
}
