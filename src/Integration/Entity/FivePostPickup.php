<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity;

use SergeR\Webasyst\AxilogSDK\Interfaces\FivepostPickupInterface;

/**
 * Class FivePostPickup
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity
 */
class FivePostPickup implements FivepostPickupInterface
{
    /** @var string */
    protected string $code = '';

    /** @var string */
    protected string $region = '';

    /** @var string */
    protected string $regionType = '';

    /** @var string */
    protected string $city = '';

    /** @var string */
    protected string $fullAddress = '';

    /** @var string */
    protected string $phone = '';

    /** @var float|null */
    protected ?float $lat = null;

    /** @var float|null */
    protected ?float $long = null;

    /** @var bool */
    protected bool $cashAllowed = false;

    /** @var bool */
    protected bool $cardAllowed = false;

    protected \SergeR\Webasyst\AxilogSDK\Integration\Entity\FivePostCellLimits $cellLimits;

    /** @var string */
    protected string $type = '';

    /** @var string */
    protected string $additional = '';

    /**
     * FivePostPickup constructor.
     */
    public function __construct()
    {
        $this->cellLimits = new FivePostCellLimits('');
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return FivePostPickup
     */
    public function setCode(string $code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegion(): string
    {
        return $this->region;
    }

    /**
     * @param string $region
     * @return FivePostPickup
     */
    public function setRegion(string $region)
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegionType(): string
    {
        return $this->regionType;
    }

    /**
     * @param string $regionType
     * @return FivePostPickup
     */
    public function setRegionType(string $regionType)
    {
        $this->regionType = $regionType;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return FivePostPickup
     */
    public function setCity(string $city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullAddress(): string
    {
        return $this->fullAddress;
    }

    /**
     * @param string $fullAddress
     * @return FivePostPickup
     */
    public function setFullAddress(string $fullAddress)
    {
        $this->fullAddress = $fullAddress;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return FivePostPickup
     */
    public function setPhone(string $phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getLat(): ?float
    {
        return $this->lat;
    }

    /**
     * @param float|null $lat
     * @return FivePostPickup
     */
    public function setLat(?float $lat)
    {
        $this->lat = $lat;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getLong(): ?float
    {
        return $this->long;
    }

    /**
     * @param float|null $long
     * @return FivePostPickup
     */
    public function setLong(?float $long)
    {
        $this->long = $long;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCashAllowed(): bool
    {
        return $this->cashAllowed;
    }

    /**
     * @param bool $cashAllowed
     * @return FivePostPickup
     */
    public function setCashAllowed(bool $cashAllowed)
    {
        $this->cashAllowed = $cashAllowed;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCardAllowed(): bool
    {
        return $this->cardAllowed;
    }

    /**
     * @param bool $cardAllowed
     * @return $this
     */
    public function setCardAllowed(bool $cardAllowed)
    {
        $this->cardAllowed = $cardAllowed;
        return $this;
    }

    /**
     * @return FivePostCellLimits
     */
    public function getCellLimits(): FivePostCellLimits
    {
        return $this->cellLimits;
    }

    /**
     * @param FivePostCellLimits $cellLimits
     * @return FivePostPickup
     */
    public function setCellLimits(FivePostCellLimits $cellLimits)
    {
        $this->cellLimits = $cellLimits;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return FivePostPickup
     */
    public function setType(string $type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getAdditional(): string
    {
        return $this->additional;
    }

    /**
     * @param string $additional
     * @return FivePostPickup
     */
    public function setAdditional(string $additional)
    {
        $this->additional = $additional;
        return $this;
    }
}
