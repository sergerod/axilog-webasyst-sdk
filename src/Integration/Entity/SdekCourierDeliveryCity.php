<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity;

use SergeR\Webasyst\AxilogSDK\Interfaces\SdekCourierDeliveryCityInterface;

/**
 * Class SdekCourierDeliveryCity
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity
 */
class SdekCourierDeliveryCity implements SdekCourierDeliveryCityInterface
{
    protected int $code = 0;

    protected string $regionName = '';

    protected string $name = '';

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return SdekCourierDeliveryCity
     */
    public function setCode(int $code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegionName(): string
    {
        return $this->regionName;
    }

    /**
     * @param string $regionName
     * @return SdekCourierDeliveryCity
     */
    public function setRegionName(string $regionName)
    {
        $this->regionName = $regionName;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return SdekCourierDeliveryCity
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }
}
