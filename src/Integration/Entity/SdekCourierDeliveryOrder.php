<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity;

use SergeR\Webasyst\AxilogSDK\Integration\Entity\DeliveryOrder\SdekStreetAddress;
use SergeR\Webasyst\AxilogSDK\Interfaces\ArraySerializable;

/**
 * Class SdekCourierDeliveryOrder
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity
 */
class SdekCourierDeliveryOrder extends SdekPickupDeliveryOrder
{
    /**
     * @var string
     */
    protected string $b_time = '';

    /**
     * @var string
     */
    protected string $e_time = '';

    /** @var SdekStreetAddress */
    protected SdekStreetAddress $address;

    public function __construct()
    {
        parent::__construct();
        $this->address = new SdekStreetAddress();
    }

    public function getPickupOfficeId(): int
    {
        return 0;
    }

    /**
     * @return string
     */
    public function getBTime(): string
    {
        return $this->b_time;
    }

    /**
     * @param string $b_time
     * @return SdekCourierDeliveryOrder
     */
    public function setBTime(string $b_time): SdekCourierDeliveryOrder
    {
        $this->b_time = $b_time;
        return $this;
    }

    /**
     * @return string
     */
    public function getETime(): string
    {
        return $this->e_time;
    }

    /**
     * @param string $e_time
     * @return SdekCourierDeliveryOrder
     */
    public function setETime(string $e_time): SdekCourierDeliveryOrder
    {
        $this->e_time = $e_time;
        return $this;
    }

    /**
     * @return SdekStreetAddress
     */
    public function getAddress(): SdekStreetAddress
    {
        return $this->address;
    }

    /**
     * @param SdekStreetAddress $address
     * @return SdekCourierDeliveryOrder
     */
    public function setAddress(SdekStreetAddress $address): SdekCourierDeliveryOrder
    {
        $this->address = clone $address;
        return $this;
    }

    public function toArray(): array
    {
        $data = parent::toArray();
        $data['address'] = $this->getAddress()->toArray();
        if ($this->getBTime()) {
            $data['@b_time'] = $this->getBTime();
        }
        if ($this->getETime()) {
            $data['@e_time'] = $this->getETime();
        }

        return $data;
    }
}
