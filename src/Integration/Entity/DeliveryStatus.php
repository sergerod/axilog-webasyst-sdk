<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2021
 * @license MIT
 */
declare(strict_types=1);

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity;
/**
 * Class DeliveryStatus
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity
 */
class DeliveryStatus
{
    /** @var null|string */
    protected $okey;

    /** @var null|string */
    protected $exe_date;

    /**
     * @var mixed
     */
    protected $order_id;
    /**
     * @var mixed
     */
    protected $order_inner_id;
    /**
     * @var float
     */
    protected $order_price;
    /**
     * @var
     */
    protected $order_customer_price;
    /**
     * @var
     */
    protected $order_incl_deliv_sum;
    /**
     * @var
     */
    protected $order_group;
    /**
     * @var
     */
    protected $order_export_order;
    /**
     * @var mixed
     */
    protected $order_type;
    /**
     * @var
     */
    protected $order_fid;
    /**
     * @var int
     */
    protected $order_payment_mode;
    /**
     * @var int
     */
    protected $status_code;
    /**
     * @var string
     */
    protected $status_text;
    /**
     * @var string
     */
    protected $d_date;
    /**
     * @var string
     */
    protected $poststatus_tracking;
    /**
     * @var float
     */
    protected $poststatus_price;

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * @return mixed
     */
    public function getOrderInnerId()
    {
        return $this->order_inner_id;
    }

    /**
     * @return float
     */
    public function getOrderPrice()
    {
        return $this->order_price;
    }

    /**
     * @return mixed
     */
    public function getOrderCustomerPrice()
    {
        return $this->order_customer_price;
    }

    /**
     * @return mixed
     */
    public function getOrderInclDelivSum()
    {
        return $this->order_incl_deliv_sum;
    }

    /**
     * @return mixed
     */
    public function getOrderGroup()
    {
        return $this->order_group;
    }

    /**
     * @return mixed
     */
    public function getOrderExportOrder()
    {
        return $this->order_export_order;
    }

    /**
     * @return mixed
     */
    public function getOrderType()
    {
        return $this->order_type;
    }

    /**
     * @return mixed
     */
    public function getOrderFid()
    {
        return $this->order_fid;
    }

    /**
     * @return int
     */
    public function getOrderPaymentMode()
    {
        return $this->order_payment_mode;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->status_code;
    }

    /**
     * @return string
     */
    public function getStatusText()
    {
        return $this->status_text;
    }

    /**
     * @return string
     */
    public function getDDate()
    {
        return $this->d_date;
    }

    /**
     * @return string
     */
    public function getPoststatusTracking()
    {
        return $this->poststatus_tracking;
    }

    /**
     * @return float
     */
    public function getPoststatusPrice()
    {
        return $this->poststatus_price;
    }

    /**
     * @param mixed $order_id
     * @return DeliveryStatus
     */
    public function setOrderId($order_id)
    {
        $this->order_id = $order_id;
        return $this;
    }

    /**
     * @param mixed $order_inner_id
     * @return DeliveryStatus
     */
    public function setOrderInnerId($order_inner_id)
    {
        $this->order_inner_id = $order_inner_id;
        return $this;
    }

    /**
     * @param float $order_price
     * @return DeliveryStatus
     */
    public function setOrderPrice(float $order_price): DeliveryStatus
    {
        $this->order_price = $order_price;
        return $this;
    }

    /**
     * @param mixed $order_customer_price
     * @return DeliveryStatus
     */
    public function setOrderCustomerPrice($order_customer_price)
    {
        $this->order_customer_price = $order_customer_price;
        return $this;
    }

    /**
     * @param mixed $order_incl_deliv_sum
     * @return DeliveryStatus
     */
    public function setOrderInclDelivSum($order_incl_deliv_sum)
    {
        $this->order_incl_deliv_sum = $order_incl_deliv_sum;
        return $this;
    }

    /**
     * @param mixed $order_group
     * @return DeliveryStatus
     */
    public function setOrderGroup($order_group)
    {
        $this->order_group = $order_group;
        return $this;
    }

    /**
     * @param mixed $order_export_order
     * @return DeliveryStatus
     */
    public function setOrderExportOrder($order_export_order)
    {
        $this->order_export_order = $order_export_order;
        return $this;
    }

    /**
     * @param mixed $order_type
     * @return DeliveryStatus
     */
    public function setOrderType($order_type)
    {
        $this->order_type = $order_type;
        return $this;
    }

    /**
     * @param mixed $order_fid
     * @return DeliveryStatus
     */
    public function setOrderFid($order_fid)
    {
        $this->order_fid = $order_fid;
        return $this;
    }

    /**
     * @param int $order_payment_mode
     * @return DeliveryStatus
     */
    public function setOrderPaymentMode(int $order_payment_mode): DeliveryStatus
    {
        $this->order_payment_mode = $order_payment_mode;
        return $this;
    }

    /**
     * @param int $status_code
     * @return DeliveryStatus
     */
    public function setStatusCode(int $status_code): DeliveryStatus
    {
        $this->status_code = $status_code;
        return $this;
    }

    /**
     * @param string $status_text
     * @return DeliveryStatus
     */
    public function setStatusText(string $status_text): DeliveryStatus
    {
        $this->status_text = $status_text;
        return $this;
    }

    /**
     * @param string $d_date
     * @return DeliveryStatus
     */
    public function setDDate(string $d_date): DeliveryStatus
    {
        $this->d_date = $d_date;
        return $this;
    }

    /**
     * @param string $poststatus_tracking
     * @return DeliveryStatus
     */
    public function setPoststatusTracking(string $poststatus_tracking): DeliveryStatus
    {
        $this->poststatus_tracking = $poststatus_tracking;
        return $this;
    }

    /**
     * @param float $poststatus_price
     * @return DeliveryStatus
     */
    public function setPoststatusPrice(float $poststatus_price): DeliveryStatus
    {
        $this->poststatus_price = $poststatus_price;
        return $this;
    }

    /**
     * @param string|null $okey
     * @return DeliveryStatus
     */
    public function setOkey(?string $okey): DeliveryStatus
    {
        $this->okey = $okey;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOkey(): ?string
    {
        return $this->okey;
    }

    /**
     * @param string|null $exe_date
     * @return DeliveryStatus
     */
    public function setExeDate(?string $exe_date): DeliveryStatus
    {
        $this->exe_date = $exe_date;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getExeDate(): ?string
    {
        return $this->exe_date;
    }
}
