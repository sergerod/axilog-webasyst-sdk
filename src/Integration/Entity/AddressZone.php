<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2020
 * @license MIT
 */

namespace SergeR\Webasyst\AxilogSDK\Integration\Entity;

/**
 * Class AddressZone
 * @package SergeR\Webasyst\AxilogSDK\Integration\Entity
 */
class AddressZone
{
    /** @var float|null */
    protected ?float $lat = null;

    /** @var float|null */
    protected ?float $long = null;

    /** @var int */
    protected int $status = 0;

    /** @var int|null */
    protected ?int $city = null;

    /** @var int|null */
    protected ?int $zone = null;

    /** @var string */
    protected string $message = "";

    /**
     * @param float|null $lat
     * @return $this
     */
    public function setLat(?float $lat): AddressZone
    {
        $this->lat = $lat;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getLat(): ?float
    {
        return $this->lat;
    }

    /**
     * @param float|null $long
     * @return $this
     */
    public function setLong(?float $long): AddressZone
    {
        $this->long = $long;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getLong(): ?float
    {
        return $this->long;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus(int $status): AddressZone
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int|null $city
     * @return $this
     */
    public function setCity(?int $city): AddressZone
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCity(): ?int
    {
        return $this->city;
    }

    /**
     * @param int|null $zone
     * @return $this
     */
    public function setZone(?int $zone): AddressZone
    {
        $this->zone = $zone;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getZone(): ?int
    {
        return $this->zone;
    }

    /**
     * @param string $message
     * @return $this
     */
    public function setMessage(string $message): AddressZone
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}
