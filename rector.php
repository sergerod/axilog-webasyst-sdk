<?php

return \Rector\Config\RectorConfig::configure()
                                  ->withPaths([__DIR__ . '/src'])
                                  ->withSets([\Rector\Set\ValueObject\LevelSetList::UP_TO_PHP_84])
                                  ->withRules([
                                      \Rector\TypeDeclaration\Rector\ClassMethod\ReturnTypeFromStrictNativeCallRector::class,
                                      \Rector\TypeDeclaration\Rector\ClassMethod\NumericReturnTypeFromStrictScalarReturnsRector::class,
                                      \Rector\TypeDeclaration\Rector\ClassMethod\StringReturnTypeFromStrictScalarReturnsRector::class,
                                      \Rector\TypeDeclaration\Rector\Function_\AddFunctionVoidReturnTypeWhereNoReturnRector::class,
                                      \Rector\TypeDeclaration\Rector\Property\TypedPropertyFromStrictConstructorRector::class,
                                      \Rector\TypeDeclaration\Rector\Property\TypedPropertyFromAssignsRector::class,
                                      \Rector\TypeDeclaration\Rector\Class_\PropertyTypeFromStrictSetterGetterRector::class,
                                      \Rector\TypeDeclaration\Rector\ClassMethod\AddParamTypeFromPropertyTypeRector::class,
                                      \Rector\TypeDeclaration\Rector\ClassMethod\ParamTypeByMethodCallTypeRector::class,
                                      \Rector\TypeDeclaration\Rector\ClassMethod\AddVoidReturnTypeWhereNoReturnRector::class,
                                      \Rector\TypeDeclaration\Rector\ClassMethod\BoolReturnTypeFromBooleanConstReturnsRector::class,
                                      \Rector\TypeDeclaration\Rector\ClassMethod\ReturnNullableTypeRector::class,
                                      \Rector\TypeDeclaration\Rector\ClassMethod\ReturnTypeFromStrictConstantReturnRector::class,
                                      \Rector\TypeDeclaration\Rector\ClassMethod\ReturnTypeFromStrictTypedCallRector::class,
                                      \Rector\TypeDeclaration\Rector\ClassMethod\ReturnTypeFromStrictParamRector::class,
                                      \Rector\TypeDeclaration\Rector\ClassMethod\ReturnTypeFromStrictTypedPropertyRector::class,
                                      \Rector\TypeDeclaration\Rector\ClassMethod\ReturnTypeFromReturnDirectArrayRector::class,
                                      \Rector\TypeDeclaration\Rector\ClassMethod\AddReturnTypeDeclarationBasedOnParentClassMethodRector::class,
                                      \Rector\CodeQuality\Rector\BooleanNot\ReplaceMultipleBooleanNotRector::class,
                                      \Rector\CodingStyle\Rector\FuncCall\CountArrayToEmptyArrayComparisonRector::class,
                                      \Rector\TypeDeclaration\Rector\ClassMethod\AddTypeFromResourceDocblockRector::class
                                  ]);
